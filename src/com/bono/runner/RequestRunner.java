package com.bono.runner;
/**
 * @author Dulan
 *
 */
// import java.util.regex.Pattern;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.junit.*;

import static org.junit.Assert.*;

import com.bono.XML_Editors.AvailabilityXML_Editor;
import com.bono.XML_Editors.CancellationXML_Editor;
import com.bono.XML_Editors.ReservationXML_Editor;
import com.bono.handlers.HttpRequestHandler;
import com.bono.handlers.OccupancyHandler;
import com.bono.loaders.DetailsLoader;
import com.bono.pojo.ModificationResponseInfo;
import com.bono.pojo.Node;
import com.bono.pojo.AvailabilityResponseInfo;
import com.bono.pojo.CancellationResponseInfo;
import com.bono.pojo.ReservationResponseInfo;
import com.bono.pojo.SearchScenario;
import com.bono.pojo.SearchType;
import com.bono.requestBuilders.AvailabilityRequest;
import com.bono.requestBuilders.CancellationRequest;
import com.bono.requestBuilders.ModificationRequest;
import com.bono.requestBuilders.ReservationRequest;
import com.bono.util.*;

public class RequestRunner {
	
	private StringBuffer                  verificationErrors = new StringBuffer();
    private HttpRequestHandler            requestHandler;
	private StringBuffer PrintWriter;
	private ReportTemplate printTemplate;
	private ArrayList<Map<Integer, String>> search_sheetlist;
	private DetailsLoader search_loader;
	//private TreeMap<Integer, SearchScenario>   searchList_Map;
	private TreeMap<Integer,Node>   availabilityList_Map;
	private TreeMap<Integer,Node>   reservationList_Map;
	private HashMap<String, SearchScenario> searchList_Map;
	private Map<Integer, String> roomDetailsMap;
	//private Map<Integer, String> map
	private int scenario = 0;
	
	
  @Before
  public void setUp() throws Exception{
	  
	  ExcelReader reader = new ExcelReader();
	  PrintWriter = new StringBuffer();
	  printTemplate = ReportTemplate.getInstance();
	  printTemplate.Initialize(PrintWriter);
	  requestHandler = new HttpRequestHandler("Ab-98");
	  search_sheetlist = new ArrayList<Map<Integer, String>>();
	  search_sheetlist = reader.init(ReadProperties.readpropety("Bonotel.ExcelScenarioPath"));
	  search_loader = new DetailsLoader();
	 
	  try {
		  searchList_Map 		= search_loader.loadBonotelSearches(search_sheetlist.get(0));
		  availabilityList_Map 	= search_loader.loadBonotel_Availabilities(search_sheetlist.get(1));
		  reservationList_Map  	= search_loader.loadBonotel_Reservations(search_sheetlist.get(2));
		  searchList_Map		= search_loader.loadRoomDetails(search_sheetlist.get(3), searchList_Map);
		  
	  } catch (Exception e) {
		  e.printStackTrace();
	  }
	  	  
	  printTemplate.markReportBegining("Bonotel Availability Check");
	
  }

  @Test
  public void testBono() throws Exception {
	  
      AvailabilityXML_Editor availableXMLEdit = new AvailabilityXML_Editor();                
	  Iterator<Map.Entry<String, SearchScenario>> iterator = (Iterator<Entry<String, SearchScenario>>) searchList_Map.entrySet().iterator();	 
	  HtmlWriter.deleteExisingFiles("Html/Request");
	  HtmlWriter.deleteExisingFiles("Html/Response");
	  
	  while (iterator.hasNext()) {
		
		  SearchScenario searchObject = iterator.next().getValue();
		  ReservationRequest reservationRequest = new ReservationRequest(reservationList_Map, requestHandler, printTemplate, PrintWriter);
		  ModificationRequest modificationRequest = new ModificationRequest(requestHandler, printTemplate, PrintWriter);
		  CancellationRequest cancellationRequest = new CancellationRequest(requestHandler, printTemplate, PrintWriter);
		  OccupancyHandler occupancyHandler =new OccupancyHandler(searchObject);
		  String occupancy = occupancyHandler.getOccupancy();
		  AvailabilityRequest.nullifyTheRequest();
		  scenario++;
		  		  
		  printTemplate.setInfoTableHeading(searchObject);
		  
		  if(searchObject.getSearchBy().equals("HOTEL ID")){
			  
		  	availableXMLEdit.change_details_HotelCode(searchObject, occupancy);		    	
		  }
		    	
		  if(searchObject.getSearchBy().equals("CITY CODE")){
			  
		    availableXMLEdit.change_details_CityCode(searchObject, occupancy);
		  }  	
		 
		  if(searchObject.getSearchBy().equals("IATA CODE")){
			  	
		    availableXMLEdit.change_details_A(searchObject, occupancy);		  
		  }
		  
		  if(searchObject.getSearchBy().equals("HOTEL GROUP CODE")){
			  
			  availableXMLEdit.change_details_H(searchObject, occupancy);
		  }
		  
		  if (searchObject.getSearchBy().equals("HOTEL ID(10)") || searchObject.getSearchBy().equals("HOTEL ID(25)")) {

			  availableXMLEdit.change_details_pack(searchObject, occupancy);
		  }
		  
		  //AvailabilityRun
		  List<Thread> threads = new ArrayList<Thread>();

		  for (int x = 0; x < Integer.parseInt(ReadProperties.readpropety("ThreadCount")); x++) {
			  Thread newThread = new Thread(AvailabilityRequest.getInstance(searchObject, requestHandler, availabilityList_Map, scenario));
			  newThread.setPriority(Thread.MAX_PRIORITY);
			  threads.add(newThread);
			  newThread.start();
		  }

		  for (Thread thread : threads) {
			  thread.join();
		  }	
		  
		  ArrayList<AvailabilityResponseInfo> availabilityList = AvailabilityRequest.getResponseList();
		  
		  //Availability Report printer
		  PrintReport printReport = new PrintReport(searchObject, printTemplate, PrintWriter);
		  printReport.getAvailabilityResponseReport(availabilityList);
		  
		  AvailabilityResponseInfo availabilityResObj = availabilityList.get(0);
		  
		  if (availabilityResObj.getStatus().equals("Y")) {
			  
			  //reservation Cancellation		  
			  if(searchObject.getSearchBy().equals("HOTEL ID")){
				  
				  if (searchObject.getUpTo().equals("R") || searchObject.getUpTo().equals("C") || searchObject.getUpTo().equals("M") || searchObject.getUpTo().equals("M&C")) {
					  
					  reservationRequest.getRoom_Reservations(searchObject, availabilityList, scenario);						
				  }				  
			  }
			  
			  //Report printers
			  if(searchObject.getSearchBy().equals("HOTEL ID")){
					
				  if (searchObject.getUpTo().equals("R") || searchObject.getUpTo().equals("C") || searchObject.getUpTo().equals("M") || searchObject.getUpTo().equals("M&C")) {
					  
					  ArrayList<ReservationResponseInfo> reservationList = reservationRequest.getReservationResponseList();	
					  printReport.getReservationResponseReport(reservationList);
					  
					  if(searchObject.getUpTo().equals("C")){
						  ArrayList<CancellationResponseInfo> cancelList = cancellationRequest.getCancellationResponseList();		  
						  printReport.getCancellationResponseReport(cancelList);
					  }
					  
					  if(searchObject.getUpTo().equals("M") || searchObject.getUpTo().equals("M&C")){
						  
						  if (searchObject.getUpTo().equals("M")) {
							  ArrayList<ModificationResponseInfo> modificationList = modificationRequest.getModificationResponseList();
							  printReport.getModificationResponseReport(modificationList);					  
						  }
						  if (searchObject.getUpTo().equals("M&C")) {
							  ArrayList<ModificationResponseInfo> modificationList = modificationRequest.getModificationResponseList();
							  printReport.getModificationResponseReport(modificationList);
							  
							  ArrayList<CancellationResponseInfo> cancelList = cancellationRequest.getCancellationResponseList();		  
							  printReport.getCancellationResponseReport(cancelList);						  
						  }
					  }				  		  
				  }
			  }	  	  
		  }else{
			  System.out.println("Response : "+availabilityResObj.getResponse()+"");
		  }
	  }	   
  }

  
  
  @After
  public void tearDown() throws Exception {
	  
	  PrintWriter.append("</body></html>");
	  BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(ReadProperties.readpropety("Bonotel.AvailabilityReportPath"))));
	  bwr.write(PrintWriter.toString());
	  bwr.flush();
	  bwr.close();
	  String verificationErrorString = verificationErrors.toString();
	  if (!"".equals(verificationErrorString)) {
		fail(verificationErrorString);
		
	  }
	  
  }
  
  
}
