package com.bono.util;

/*
 * By Dulan
 * Modified Date : 2013.03.11
 * 
 */
import java.io.*;

public class HtmlWriter {
	public static PrintStream orgStream;

	public HtmlWriter() {
		orgStream = System.out;
	}

	// sample Comment
	public void Writetofile(String path, boolean flag) {
		// orgStream = null;
		PrintStream fileStream = null;
		try {

			// orgStream = System.out;
			fileStream = new PrintStream(new FileOutputStream(path, flag));
			System.setOut(fileStream);
			// Redirecting runtime exceptions to file
			System.setErr(fileStream);

			throw new Exception("Test Exception");

		} catch (FileNotFoundException fnfEx) {
			// System.out.println("Error in IO Redirection");
			fnfEx.printStackTrace();
		} catch (Exception ex) {
			// Gets printed in the file
			System.out.println();
			// ex.printStackTrace();
		} finally {
			// Restoring back to console
			// System.setOut(orgStream);
			// Gets printed in the console
			// System.out.println("Redirecting file output back to console");

		}

	}

	
	public static void setToDefault() {

		System.setOut(orgStream);
	}
	
	public static void getResponseFileWrite(String path, String content){
		
		FileOutputStream fop = null;
		File file;
		
		try {

			file = new File(path);
			fop = new FileOutputStream(file);
			
			if (!file.exists()) {
				file.createNewFile();
			}else{
				file.delete();
				file.createNewFile();
			}

			// get the content in bytes
			byte[] contentInBytes = content.getBytes();

			fop.write(contentInBytes);
			fop.flush();
			fop.close();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}		
	}
	
	public static void deleteExisingFiles(String path) {
		
		File file = new File(path);
		String[] myFiles;
		if (file.isDirectory()) {
			myFiles = file.list();
			for (int i = 0; i < myFiles.length; i++) {
				File myFile = new File(file, myFiles[i]);
				myFile.delete();
			}
		}
	}
	
}