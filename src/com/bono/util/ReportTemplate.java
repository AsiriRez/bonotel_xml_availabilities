package com.bono.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.bono.pojo.SearchScenario;

public class ReportTemplate {
	
	private StringBuffer    PrintWriter           = null;
	private static int      TestCaseCount;
	//private HashMap<String, String>   PropertySet = null;
	    
	public void Initialize(StringBuffer printer)
	{
		this.PrintWriter = printer;
	    // TestCaseCount    = ReservationScenarioRunner.TestCaseCount;
	}
	
	private static class SingletonHolder { 
	    private static final ReportTemplate INSTANCE = new ReportTemplate();
	}
	
	public static ReportTemplate getInstance() {
	    return SingletonHolder.INSTANCE;
	}
	
	public void setTableHeading(String TableHeader){
		  
		PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">"+TableHeader+"</p><br>");
		PrintWriter.append("<table ><tr><th>TestNode</th><th>URL</th><th>Availability</th><th>Number of Hotels</th><th>Elapsed Time</th><th>Test Status</th><th>Description</th></tr>");		 	  
	}
	
	public void setRCTableHeading(String TableHeader){
		  
		PrintWriter.append("<br><br><p class='InfoSup' style=\"font-weight: bold;\">"+TableHeader+"</p><br>");
		PrintWriter.append("<table ><tr><th>TestNode</th><th>URL</th><th>Expected</th><th>Actual</th><th>Elapsed Time</th><th>Test Status</th><th>Description</th></tr>");		 	  
	}
	  
	public void markTableEnd(){
		  
		PrintWriter.append("</table>");		  
	}
	  
	public void markReportBegining(String reportHeadding){
		
		DateFormat dateFormatcurrentDate = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = Calendar.getInstance();
		String currentDateforMatch = dateFormatcurrentDate.format(cal.getTime()); 
		  
	    PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
	    PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>"+reportHeadding+" ("+currentDateforMatch+")</p></div>");
	    PrintWriter.append("<body>");	    
	}
	  
	public void setInfoTableHeading(SearchScenario searchObject){
		
		String[] adultsList = searchObject.getRoomAD().split("/");
		String[] childList = searchObject.getRoomCH().split("/");	
		
		PrintWriter.append("<br><br>");
		PrintWriter.append("<div style=\"border:none;border-radius:4px 4px 4px 4px;background-color:#F1F1ED;padding-bottom: 4px;padding-top: 4px; width:50%;\">");
	  	PrintWriter.append("<p class='InfoSub'>------------------------Search By "+searchObject.getSearchBy()+"---------------------------------------------------</p>");
	  	PrintWriter.append("<p class='InfoSub'>Current Search Crieteria</p>");
	  	PrintWriter.append("<p class='InfoSub'>Check in date   : "+searchObject.getCheckInDate()+"</p>");
	  	PrintWriter.append("<p class='InfoSub'>Check out date  : "+searchObject.getCheckOutDate()+"</p>");
	  	PrintWriter.append("<p class='InfoSub'>Country         : US </p>");
	  	PrintWriter.append("<p class='InfoSub'>"+searchObject.getSearchBy()+"        : "+searchObject.getCode()+"</p>");
	  	PrintWriter.append("<p class='InfoSub'>TourOperator    : "+searchObject.getTouroperator()+"</p>");
	  	PrintWriter.append("<p class='InfoSub'>Validation type : "+searchObject.getValidationType()+"</p>");
	  	PrintWriter.append("<p class='InfoSub'>Modification type : "+searchObject.getModificationType()+"</p>");
	  	PrintWriter.append("<p class='InfoSub'>Reservation Deatils :- Number Of rooms "+searchObject.getNoOfRooms()+" For: " +searchObject.getNoOfNights()+" Nights</p>");
	  	
	  	for (int i = 1; i <= Integer.parseInt(searchObject.getNoOfRooms()); i++) {
	  		PrintWriter.append("<p class='InfoSub'>Occupancy: (Room "+i+") Adults:- "+adultsList[i-1]+" Child Count:- "+childList[i-1]+"</p>");			  
		}
	  	
	  	PrintWriter.append("</div>");
	  	PrintWriter.append("<br>");		
	}
	  
	public void addToInfoTabel(String Attr,String Parameter){
	
		PrintWriter.append("<tr><td>"+Attr+"</th><td>"+Parameter+"</th></tr>");		  
	}

}
