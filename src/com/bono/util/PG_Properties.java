package com.bono.util;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

public class PG_Properties {

	private static final String RELATIVE_URL = "Config/Prop.properties";
	private static FileReader reader;

	public static void main(String[] args) {

		System.out.println(getProperty("Proxy.Port"));
	}

	public static String getProperty(String Key) {
		Properties prop = new Properties();

		try {
			reader = new FileReader(new File(RELATIVE_URL));
			prop.load(reader);
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		return prop.getProperty(Key);

	}

}
