package com.bono.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.bono.pojo.AvailabilityResponseInfo;
import com.bono.pojo.CancellationResponseInfo;
import com.bono.pojo.ModificationResponseInfo;
import com.bono.pojo.ReservationResponseInfo;
import com.bono.pojo.RoomType;
import com.bono.pojo.SearchScenario;

public class PrintReport {
	
	private ReportTemplate printTemplate;
	private StringBuffer PrintWriter;
	private SearchScenario searchObject;

	public PrintReport(SearchScenario searchObj, ReportTemplate printTemp, StringBuffer PrintWrite){
		this.searchObject = searchObj;
		this.printTemplate = printTemp;
		this.PrintWriter = PrintWrite;			
	}
	
	public void getAvailabilityResponseReport(ArrayList<AvailabilityResponseInfo> listResponse){
		
		try {			
			printTemplate.setTableHeading("--- Availability Request ---");
			
			for (int i = 0; i < listResponse.size(); i++) {
				
				PrintWriter.append("<tr><td>" + listResponse.get(i).getAvailability_Note()+ "</td><td>" + listResponse.get(i).getAvailability_URL() + "</td>");				
				
				if (listResponse.get(i).getStatus().equalsIgnoreCase("Y")) {
					
					if (searchObject.getValidationType().equals("Normal")) {
						
						PrintWriter.append("<td>Available</td><td>"+ listResponse.get(i).getHotelcount() + "</td><td>" + listResponse.get(i).getElapsedtime()+ "ms</td>");
						PrintWriter.append("<td class='Passed'>PASS</td>");
					
						try {						
							if (!(listResponse.get(i).getBenchMarkContent().equalsIgnoreCase(listResponse.get(i).getPageSource())))
								PrintWriter.append("<td>Warning:Page Content Not Same</td></tr>");
							else
								PrintWriter.append("<td>-</td></tr>");
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						printTemplate.markTableEnd();
						
						printTemplate.setRCTableHeading("--- Availability Response Rates ---");
						
						ArrayList<String> changeRoom_room_No	= listResponse.get(i).getChangeRoom_room_No();
						ArrayList<String> changeRoom_total_rate = listResponse.get(i).getChangeRoom_total_rate();
						ArrayList<String> changeRoom_room_type	= listResponse.get(i).getChangeRoom_room_type();
						ArrayList<String> changeRoom_bed_type	= listResponse.get(i).getChangeRoom_bed_type();
						ArrayList<String> changeRoom_Rate_plan	= listResponse.get(i).getChangeRoom_Rate_plan();
						
						int noOfRooms = Integer.valueOf(searchObject.getNoOfRooms());
						
						String roomType = "";
						String bedType 	= "";
						String ratePaln = "";
						String rate		= "";
						
						System.out.println("============STARTING VALIDATION RATES========");
						
						for(int r = 1; r<=noOfRooms; r++)
						{
							for(int q=0; q<changeRoom_room_No.size(); q++)
							{
								System.out.println(String.valueOf(r));
								System.out.println("q = "+q);
								System.out.println(changeRoom_room_No.get(r));
								
								if(changeRoom_room_No.get(q).equals(String.valueOf(r)))
								{
									roomType	= changeRoom_room_type.get(q);
									bedType		= changeRoom_bed_type.get(q);
									ratePaln	= changeRoom_Rate_plan.get(q);
									rate		= changeRoom_total_rate.get(q);
									
									ArrayList<RoomType> roomList = searchObject.getResultsRoomMap().get(String.valueOf(r));
									
									for(int v=0; v<roomList.size(); v++)
									{
										System.out.println(roomType+" = "+roomList.get(v).getRoomType());
										System.out.println(bedType+" = "+roomList.get(v).getBedType());
										System.out.println(ratePaln+" = "+roomList.get(v).getRateType());
										System.out.println(rate+" = "+roomList.get(v).getTotalRate());
										
										if( roomType.equals(roomList.get(v).getRoomType()) && bedType.equals(roomList.get(v).getBedType()) && ratePaln.equals(roomList.get(v).getRateType()) )
										{
											System.out.println( changeRoom_total_rate.get(q)+ " = " +roomList.get(v).getTotalRate() );
											
											PrintWriter.append("<tr><td>" + v + "</td><td>" + roomType+bedType+ratePaln + "</td>");
											PrintWriter.append("<td>"+changeRoom_total_rate.get(q)+"</td>");
											PrintWriter.append("<td>"+roomList.get(v).getTotalRate() +"</td>");
											PrintWriter.append("<td></td>");
											if(rate.equals(roomList.get(v).getTotalRate()))
											{
												PrintWriter.append("<td class='Passed'>PASS</td>");
											}
											else
											{
												PrintWriter.append("<td class='Failed'>FAIL</td>");
											}
											PrintWriter.append("<td></td></tr>");
										}
									}
								}
							}
						}
						printTemplate.markTableEnd();
						
					}else{
						PrintWriter.append("<td>Available</td><td>"+ listResponse.get(i).getHotelcount() + "</td><td>" + listResponse.get(i).getElapsedtime()+ "ms</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><td>Blackout or Cutoff period Results  Available</td></tr>");
						
					}
					
				}else{
					
					if (searchObject.getValidationType().equals("Blackout")|| searchObject.getValidationType().equals("Cutoff")) {
						PrintWriter.append("<td>Not Available</td><td>0</td><td>-</td>");
						PrintWriter.append("<td class='Passed'>PASS</td><td>Blackout or Cutoff period Results Not Available</td></tr>");

					} else {
						PrintWriter.append("<td>Not Available</td><td>0</td><td>-</td>");
						PrintWriter.append("<td class='Failed'>FAIL</td><td>"+ listResponse.get(i).getErrorCode() + ":" + listResponse.get(i).getErrorDiscription()+ "</td></tr>");
					}
					
				}
			}
			
			printTemplate.markTableEnd();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public void getReservationResponseReport(ArrayList<ReservationResponseInfo> listResponse){
		
		try {
			printTemplate.setRCTableHeading("--- Reservation Request ---");
			
			for (int i = 0; i < listResponse.size(); i++) {
				
				PrintWriter.append("<tr><td>" + listResponse.get(i).getNode()+ "</td><td>" + listResponse.get(i).getUrl() + "</td>");
								
				if (listResponse.get(i).getStatus().equalsIgnoreCase("Y")) {
					
					PrintWriter.append("<td>True</td><td>True</td><td>" + listResponse.get(i).getElapsedtime()+ "ms</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><td>"+ listResponse.get(i).getBookingNo()+"</td></tr>");
				}else{
					PrintWriter.append("<td>True</td><td>False</td><td>-</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><td>"+ listResponse.get(i).getErrorCode() + ":" + listResponse.get(i).getErrorDescription()+ "</td></tr>");						
				}
			}
			
			printTemplate.markTableEnd();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void getCancellationResponseReport(ArrayList<CancellationResponseInfo> listResponse){
		
		try {
			printTemplate.setRCTableHeading("--- Cancellation Request ---");
			
			for (int i = 0; i < listResponse.size(); i++) {
				
				PrintWriter.append("<tr><td>" + listResponse.get(i).getNode()+ "</td><td>" + listResponse.get(i).getUrl() + "</td>");
								
				if (listResponse.get(i).getStatus().equalsIgnoreCase("Y")) {
					
					PrintWriter.append("<td>True</td><td>True</td><td>" + listResponse.get(i).getElapsedtime()+ "ms</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><td>"+ listResponse.get(i).getCancellationNo()+"</td></tr>");
				}else{
					PrintWriter.append("<td>True</td><td>False</td><td>-</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><td>"+ listResponse.get(i).getErrorCode() + ":" + listResponse.get(i).getErrorDescription()+ "</td></tr>");						
				}
			}
			
			printTemplate.markTableEnd();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void getModificationResponseReport(ArrayList<ModificationResponseInfo> listResponse){
		
		try {
			printTemplate.setRCTableHeading("--- Modification Request ---");
			
			for (int i = 0; i < listResponse.size(); i++) {
				
				PrintWriter.append("<tr><td>" + listResponse.get(i).getNode()+ "</td><td>" + listResponse.get(i).getUrl() + "</td>");
								
				if (listResponse.get(i).getStatus().equalsIgnoreCase("Y")) {
					
					PrintWriter.append("<td>True</td><td>True</td><td>" + listResponse.get(i).getElapsedtime()+ "ms</td>");
					PrintWriter.append("<td class='Passed'>PASS</td><td>"+ listResponse.get(i).getModreferenceNo()+"</td></tr>");
				}else{
					PrintWriter.append("<td>True</td><td>False</td><td>-</td>");
					PrintWriter.append("<td class='Failed'>FAIL</td><td>"+ listResponse.get(i).getErrorCode() + ":" + listResponse.get(i).getErrorDescription()+ "</td></tr>");						
				}
			}
			
			printTemplate.markTableEnd();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
}
