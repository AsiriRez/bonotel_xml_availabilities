package com.bono.requestBuilders;

import java.util.ArrayList;

import org.apache.commons.lang3.time.StopWatch;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.bono.XML_Editors.ModificationXML_Editor;
import com.bono.handlers.HttpRequestHandler;
import com.bono.pojo.AvailabilityResponseInfo;
import com.bono.pojo.CancellationResponseInfo;
import com.bono.pojo.ModificationResponseInfo;
import com.bono.pojo.Node;
import com.bono.pojo.ReservationResponseInfo;
import com.bono.pojo.SearchScenario;
import com.bono.util.HtmlWriter;
import com.bono.util.ReportTemplate;

public class ModificationRequest {

	private ReportTemplate printTemplate;
	private StringBuffer PrintWriter;
	private static HttpRequestHandler requestHandler;
	private static ArrayList<ModificationResponseInfo> modifyResponseList = null;

	public ModificationRequest(HttpRequestHandler requestHandle, ReportTemplate printTemp, StringBuffer PrintWrite){
		this.printTemplate = printTemp;
		this.PrintWriter = PrintWrite;	
		this.requestHandler = requestHandle;	
		modifyResponseList = new ArrayList<ModificationResponseInfo>();
	}
	
	public static void getRoom_Modifications(SearchScenario searchObject, ReservationResponseInfo reservationResponseInfo, Node reservationNodes, int scenario, ArrayList<AvailabilityResponseInfo> availableResponseInfoList) throws InterruptedException {
		
		ModificationResponseInfo modificationResponse = new ModificationResponseInfo();
		ModificationXML_Editor modificationXMLEdit = new ModificationXML_Editor(requestHandler);
		
		modificationXMLEdit.getRoomsXMLModifications(searchObject, reservationResponseInfo, availableResponseInfoList);
		
		String ErrorCode = "";
		String ErrorDescription = "";

		String modifyURL = reservationNodes.getURL().replace("GetReservation.do", "GetModifyReservation.do");
		modificationResponse.setUrl(modifyURL);
		modificationResponse.setNode(reservationNodes.getNode());
		
		String Value = "xml=".concat(modificationXMLEdit.parser());
		HtmlWriter.getResponseFileWrite("Html/Request/"+scenario+"_"+reservationNodes.getNode()+"_Modification_Request.txt", Value);
		
		StopWatch st = new StopWatch();
		st.start();
		String Response = null;
		
		try {
			Response = requestHandler.sendPost(Value.replace(" ", ""), modifyURL);
			HtmlWriter.getResponseFileWrite("Html/Response/"+scenario+"_"+reservationNodes.getNode()+"_Modification_Response.txt", Response);
			System.out.println("Response : " + Response);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		Document doc = Jsoup.parse(Response);
		
		try {
			
			String status = doc.getElementsByTag("modifyReservationResponse").first().attr("status");
			modificationResponse.setStatus(status);
			st.stop();
			long elapsedtime = st.getTime();
			modificationResponse.setElapsedtime(elapsedtime);
			st.reset();
			String PageSource = doc.toString();
			
			if (status.equals("Y")) {
				
				String modreferenceNo = doc.getElementsByTag("referenceNo").text();
				modificationResponse.setModreferenceNo(modreferenceNo);
				String modFee = doc.getElementsByTag("modificationFee").text();
				modificationResponse.setModification_Fee(modFee);
									
			}else{
				
				ErrorCode = doc.getElementsByTag("code").text();
				ErrorDescription = doc.getElementsByTag("description").text();
				modificationResponse.setErrorCode(ErrorCode);
				modificationResponse.setErrorDescription(ErrorDescription);
			}
			
		} catch (Exception e) {
			e.getMessage();
		}
		
		modifyResponseList.add(modificationResponse);
	}
	
	public ArrayList<ModificationResponseInfo> getModificationResponseList(){	
		
		try {			
			if (!(modifyResponseList.isEmpty())) {
				return modifyResponseList;
			}else{
				return null;
			}
			
		} catch (Exception e) {
			return null;
		}
		
		
	}
}
