package com.bono.requestBuilders;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.commons.lang3.time.StopWatch;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.bono.XML_Editors.ReservationXML_Editor;
import com.bono.handlers.HttpRequestHandler;
import com.bono.pojo.*;
import com.bono.util.HtmlWriter;
import com.bono.util.ReportTemplate;

public class ReservationRequest {
	
	private ReportTemplate printTemplate;
	private StringBuffer PrintWriter;
	private HttpRequestHandler requestHandler;
	private TreeMap<Integer, Node> reservationList_Map;
	private ArrayList<ReservationResponseInfo> responseList = new ArrayList<ReservationResponseInfo>();

	public ReservationRequest(TreeMap<Integer, Node> reservation_Map, HttpRequestHandler requestHandle, ReportTemplate printTemp, StringBuffer PrintWrite){
		this.printTemplate = printTemp;
		this.PrintWriter = PrintWrite;	
		this.requestHandler = requestHandle;
		this.reservationList_Map = reservation_Map;
	}
	
	public void getRoom_Reservations(SearchScenario searchObject, ArrayList<AvailabilityResponseInfo> availableResponseInfoList, int scenario) throws InterruptedException {
		
		Iterator<Map.Entry<Integer,Node>> reservationIterator = (Iterator<Entry<Integer,Node>>) reservationList_Map.entrySet().iterator();
		
		while (reservationIterator.hasNext()) {
			
			ReservationXML_Editor reservationXMLEdit = new ReservationXML_Editor(availableResponseInfoList);
			ReservationResponseInfo reservationResponseInfo = new ReservationResponseInfo();
			
			reservationXMLEdit.getRoomDataInfoLoaded(searchObject);			
			Node reservationNodes = reservationIterator.next().getValue();
			
			reservationResponseInfo.setNode(reservationNodes.getNode());
			reservationResponseInfo.setUrl(reservationNodes.getURL());
			
			String ErrorCode = "";
			String ErrorDescription = "";

			String Value = "xml=".concat(reservationXMLEdit.parser());
			HtmlWriter.getResponseFileWrite("Html/Request/"+scenario+"_"+reservationNodes.getNode()+"_Reservation_Request.txt", Value);
			
			StopWatch st = new StopWatch();
			st.start();
			String Response = null;

			try {
				Response = requestHandler.sendPost(Value.replace(" ", ""), reservationNodes.getURL());
				HtmlWriter.getResponseFileWrite("Html/Response/"+scenario+"_"+reservationNodes.getNode()+"_Reservation_Response.txt", Response);
				System.out.println("Response : " + Response);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			Document doc = Jsoup.parse(Response);			
			String status = null;
			
			try {
				status = doc.getElementsByTag("reservationresponse").first().attr("status");
				reservationResponseInfo.setStatus(status);
				st.stop();
				long elapsedtime = st.getTime();
				reservationResponseInfo.setElapsedtime(elapsedtime);
				st.reset();
				String PageSource = doc.toString();
				
				ArrayList<String> roomResNoList = new ArrayList<String>();
				
				if (status.equals("Y")) {
					
					String reservationNo = doc.getElementsByTag("referenceNo").text();
					reservationResponseInfo.setBookingNo(reservationNo);
					String totalBookValue = doc.getElementsByTag("total").text();
					reservationResponseInfo.setTotalBookingValue(totalBookValue);	
					
					List<Element> roomResElements = doc.getElementsByTag("roomResNo");
					
					for (Element e : roomResElements) {
						roomResNoList.add(e.text());
					}
					
					reservationResponseInfo.setRoomResNo(roomResNoList);
				
				}else{
					ErrorCode = doc.getElementsByTag("code").text();
					ErrorDescription = doc.getElementsByTag("description").text();
					reservationResponseInfo.setErrorCode(ErrorCode);
					reservationResponseInfo.setErrorDescription(ErrorDescription);					
				}
				
			} catch (Exception e) {
				e.toString();
			}
			
			
			if (status.equals("Y")) {
				
				//Modifications
				if(searchObject.getUpTo().equals("M") || searchObject.getUpTo().equals("M&C")){
					  
					if (searchObject.getUpTo().equals("M")) {
						ModificationRequest.getRoom_Modifications(searchObject, reservationResponseInfo, reservationNodes, scenario, availableResponseInfoList);
					}
					if (searchObject.getUpTo().equals("M&C")) {
						ModificationRequest.getRoom_Modifications(searchObject, reservationResponseInfo, reservationNodes, scenario, availableResponseInfoList);
						CancellationRequest.getRoom_Cancellation(searchObject, reservationResponseInfo, reservationNodes, scenario);	  
					}
				}
				
				//Cancellation
				if (searchObject.getUpTo().equals("C")) {
									
					CancellationRequest.getRoom_Cancellation(searchObject, reservationResponseInfo, reservationNodes, scenario);
				}
			}
						
			responseList.add(reservationResponseInfo);
			
		}
		
				
	}
	
	
	public ArrayList<ReservationResponseInfo> getReservationResponseList(){		
		return responseList;
	}
}
