package com.bono.requestBuilders;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.commons.lang3.time.StopWatch;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.bono.XML_Editors.CancellationXML_Editor;
import com.bono.XML_Editors.ReservationXML_Editor;
import com.bono.handlers.HttpRequestHandler;
import com.bono.pojo.CancellationResponseInfo;
import com.bono.pojo.Node;
import com.bono.pojo.ReservationResponseInfo;
import com.bono.pojo.SearchScenario;
import com.bono.util.HtmlWriter;
import com.bono.util.ReportTemplate;

public class CancellationRequest {
	
	private ReportTemplate printTemplate;
	private StringBuffer PrintWriter;
	private static HttpRequestHandler requestHandler;
	private static ArrayList<CancellationResponseInfo> cancelResponseList = null;

	public CancellationRequest(HttpRequestHandler requestHandle, ReportTemplate printTemp, StringBuffer PrintWrite){
		this.printTemplate = printTemp;
		this.PrintWriter = PrintWrite;	
		this.requestHandler = requestHandle;	
		cancelResponseList = new ArrayList<CancellationResponseInfo>();
	}
	
	public static void getRoom_Cancellation(SearchScenario searchObject, ReservationResponseInfo reservationResponseInfo, Node reservationNodes, int scenario) throws InterruptedException {
	
		CancellationResponseInfo cancellationResponseInfo = new CancellationResponseInfo();				
		CancellationXML_Editor cancellationXMLEdit = new CancellationXML_Editor();
		
		cancellationXMLEdit.getRoomsXMLCancellations(searchObject, reservationResponseInfo);
				
		String ErrorCode = "";
		String ErrorDescription = "";

		String cancelURL = reservationNodes.getURL().replace("GetReservation.do", "GetCancellation.do");
		cancellationResponseInfo.setUrl(cancelURL);
		cancellationResponseInfo.setNode(reservationNodes.getNode());
		
		String Value = "xml=".concat(cancellationXMLEdit.parser());
		HtmlWriter.getResponseFileWrite("Html/Request/"+scenario+"_"+reservationNodes.getNode()+"_Cancellation_Request.txt", Value);
		
		StopWatch st = new StopWatch();
		st.start();
		String Response = null;
		
		try {
			Response = requestHandler.sendPost(Value.replace(" ", ""), cancelURL);
			HtmlWriter.getResponseFileWrite("Html/Response/"+scenario+"_"+reservationNodes.getNode()+"_Cancellation_Response.txt", Response);
			System.out.println("Response : " + Response);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		Document doc = Jsoup.parse(Response);
		
		try {
			
			String status = doc.getElementsByTag("cancellationResponse").first().attr("status");
			cancellationResponseInfo.setStatus(status);
			st.stop();
			long elapsedtime = st.getTime();
			cancellationResponseInfo.setElapsedtime(elapsedtime);
			st.reset();
			String PageSource = doc.toString();
			
			if (status.equals("Y")) {
				
				String cancellationNo = doc.getElementsByTag("cancellationNo").text();
				cancellationResponseInfo.setCancellationNo(cancellationNo);
				String cancellationFee = doc.getElementsByTag("canellationFee").text();
				cancellationResponseInfo.setCancellation_Fee(cancellationFee);
									
			}else{
				
				ErrorCode = doc.getElementsByTag("code").text();
				ErrorDescription = doc.getElementsByTag("description").text();
				cancellationResponseInfo.setErrorCode(ErrorCode);
				cancellationResponseInfo.setErrorDescription(ErrorDescription);
			}
							
		} catch (Exception e) {
			e.toString();
		}
				
		cancelResponseList.add(cancellationResponseInfo);
				
	}
	
	public ArrayList<CancellationResponseInfo> getCancellationResponseList(){	
		
		try {			
			if (!(cancelResponseList.isEmpty())) {
				return cancelResponseList;
			}else{
				return null;
			}
			
		} catch (Exception e) {
			return null;
		}
		
		
	}
}
