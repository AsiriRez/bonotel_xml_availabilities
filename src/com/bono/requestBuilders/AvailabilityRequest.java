package com.bono.requestBuilders;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.commons.lang3.time.StopWatch;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.bono.XML_Editors.*;
import com.bono.handlers.HttpRequestHandler;
import com.bono.pojo.*;
import com.bono.util.HtmlWriter;
import com.bono.util.ReportTemplate;

public class AvailabilityRequest implements Runnable{
	
	private int hotelcount;
	private int benchmarkCount;
	private boolean resultsRecorded = false;
	private boolean CountRecorded = false;	
	private static SearchScenario searchObject;
	private HttpRequestHandler requestHandler;
	private static AvailabilityRequest  request = null;
	private TreeMap<Integer, Node> availabilityList_Map;
	private Iterator<Map.Entry<Integer, Node>> availabilityIterator;
	private static ArrayList<AvailabilityResponseInfo> responseList = null;
	private Queue<Node> nodeList; 
	private int scenario;
	
	private AvailabilityRequest(SearchScenario searchObj, HttpRequestHandler reqHandler, TreeMap<Integer, Node> availability_Map, int scenarioCnt){
		this.searchObject = searchObj;
		this.requestHandler = reqHandler;
		this.availabilityList_Map = availability_Map;
		this.scenario = scenarioCnt;
		availabilityIterator = (Iterator<Entry<Integer, Node>>) availabilityList_Map.entrySet().iterator();
		
		nodeList = new ConcurrentLinkedQueue<Node>();
		responseList = new ArrayList<AvailabilityResponseInfo>();	
		
		while (availabilityIterator.hasNext()) {
			Node node = availabilityIterator.next().getValue();
			nodeList.add(node);			
		}		
	}
	
	public static AvailabilityRequest getInstance(SearchScenario searchObj, HttpRequestHandler ReqHandler, TreeMap<Integer, Node> availability_Map, int scenarioCnt){
		
		if (request==null) {
			request = new AvailabilityRequest(searchObj, ReqHandler, availability_Map, scenarioCnt);
		}	
		return request;
	}
	
	public static void  nullifyTheRequest(){
		request = null;
	}
	
	@Override
	public void run() {
		try {
			request.getRooms_Availabilities();
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	public static ArrayList<AvailabilityResponseInfo> getResponseList() {		
		return responseList;
	}
	
	
	public void getRooms_Availabilities() throws InterruptedException {

		AvailabilityXML_Editor Xl = new AvailabilityXML_Editor();
	    String BenchMarkContent = null;
	    
		while (!(nodeList.isEmpty())) {
			
			AvailabilityResponseInfo availableResponseInfo = new AvailabilityResponseInfo();
			Node availableNode = nodeList.poll();
			availableResponseInfo.setAvailability_Note(availableNode.getNode());
			availableResponseInfo.setAvailability_URL(availableNode.getURL());
			
            String ErrorCode = "";
			String ErrorDescription = "";
            String Value = "xml=".concat(Xl.parser()).replace("<city>0</city>","");
            HtmlWriter.getResponseFileWrite("Html/Request/"+scenario+"_"+availableNode.getNode()+"_Availability_Request.txt", Value);

			StopWatch st = new StopWatch();
			st.start();
			String Response = null;

			try {
				Response = requestHandler.sendPost(Value.replace(" ", ""), availableNode.getURL());
				availableResponseInfo.setResponse(Response);
				HtmlWriter.getResponseFileWrite("Html/Response/"+scenario+"_"+availableNode.getNode()+"Availability_Response.txt", Response);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			Document doc = Jsoup.parse(Response);
			String status = null;
						
			try {
				status = doc.getElementsByTag("availabilityresponse").first().attr("status");
				availableResponseInfo.setStatus(status);
				st.stop();
				long elapsedtime = st.getTime();
				availableResponseInfo.setElapsedtime(elapsedtime);
				st.reset();
				String PageSource = doc.toString();
				availableResponseInfo.setElapsedtime(elapsedtime);

				if (BenchMarkContent == null)
					BenchMarkContent = PageSource;
					availableResponseInfo.setPageSource(PageSource);
					availableResponseInfo.setBenchMarkContent(BenchMarkContent);

				if (status.equals("Y")) {

					if (searchObject.getValidationType().equals("Normal")) {

						hotelcount = doc.getElementsByTag("hotel").size();
						availableResponseInfo.setHotelcount(hotelcount);
									
						try {
							
							Elements hotels = null;
							ArrayList<String> room_No = new ArrayList<String>();
							ArrayList<String> room_code = new ArrayList<String>();
							ArrayList<String> room_typecode = new ArrayList<String>();
							ArrayList<String> room_type = new ArrayList<String>();
							ArrayList<String> bed_typecode = new ArrayList<String>();
							ArrayList<String> bed_type = new ArrayList<String>();
							ArrayList<String> rate_plancode = new ArrayList<String>();
							ArrayList<String> rate_plan = new ArrayList<String>();
							ArrayList<String> totalRate = new ArrayList<String>();
							ArrayList<String> changeRoom_room_No = new ArrayList<String>();
							ArrayList<String> changeRoom_total_rate = new ArrayList<String>();
							ArrayList<String> changeRoom_room_code = new ArrayList<String>();
							ArrayList<String> changeRoom_room_typecode = new ArrayList<String>();
							ArrayList<String> changeRoom_room_type = new ArrayList<String>();
							ArrayList<String> changeRoom_bed_typecode = new ArrayList<String>();
							ArrayList<String> changeRoom_bed_type = new ArrayList<String>();
							ArrayList<String> changeRoom_Rate_plancode = new ArrayList<String>();
							ArrayList<String> changeRoom_Rate_plan = new ArrayList<String>();
							
							String genHotelCode = "";
							List<Element> hotelElements = doc.getElementsByTag("hotel");
							
							if (searchObject.getSearchBy().equals("HOTEL ID")) {
								genHotelCode = searchObject.getCode();
							}else if(searchObject.getSearchBy().equals("CITY CODE") || searchObject.getSearchBy().equals("IATA CODE")){
								
								for (Element e : hotelElements){
									genHotelCode = e.getElementsByTag("hotelCode").text();
									break;
								}
							}
							
							availableResponseInfo.setHotel_id(genHotelCode);
							
							outerloop : for (Element e : hotelElements){
								
								String xmlHotelCode = e.getElementsByTag("hotelCode").text();
								
								if (xmlHotelCode.equals(genHotelCode)) {
									
									List<Element> roomElement = e.getElementsByTag("roomNo");
									
									for (int i = 1; i <= Integer.parseInt(searchObject.getNoOfRooms()); i++) {							
										innerloop : for (int j = 0; j < roomElement.size(); j++) {
											
											if(i == Integer.parseInt(roomElement.get(j).text())){
												
												room_No.add(roomElement.get(j).text());
												
												List<Element> r_codeList = e.getElementsByTag("roomCode");
												room_code.add(r_codeList.get(j).text());
												
												List<Element> r_typecodeList = e.getElementsByTag("roomTypeCode");
												room_typecode.add(r_typecodeList.get(j).text());
												
												List<Element> r_typeList = e.getElementsByTag("roomType");
												room_type.add(r_typeList.get(j).text());
												
												List<Element> b_typecodeList = e.getElementsByTag("bedTypeCode");
												bed_typecode.add(b_typecodeList.get(j).text());
												
												List<Element> b_typeList = e.getElementsByTag("bedType");
												bed_type.add(b_typeList.get(j).text());
												
												List<Element> r_planCodeList = e.getElementsByTag("ratePlanCode");
												rate_plancode.add(r_planCodeList.get(j).text());
												
												List<Element> r_planList = e.getElementsByTag("ratePlan");
												rate_plan.add(r_planList.get(j).text());
												
												List<Element> r_totalRateList = e.getElementsByTag("totalRate");
												totalRate.add(r_totalRateList.get(j).text().replace(",", ""));
																					
												break innerloop;
											}
										}														
									}
									
									for (int i = 1; i <= Integer.parseInt(searchObject.getNoOfRooms()); i++) {							
										for (int j = 0; j < roomElement.size(); j++) {
											
											if(i == Integer.parseInt(roomElement.get(j).text())){
												
												changeRoom_room_No.add(roomElement.get(j).text());
											
												List<Element> r_codeList = e.getElementsByTag("roomCode");
												changeRoom_room_code.add(r_codeList.get(j).text());
												
												List<Element> r_typecodeList = e.getElementsByTag("roomTypeCode");
												changeRoom_room_typecode.add(r_typecodeList.get(j).text());
												
												List<Element> r_typeList = e.getElementsByTag("roomType");
												changeRoom_room_type.add(r_typeList.get(j).text());
												
												List<Element> b_typecodeList = e.getElementsByTag("bedTypeCode");
												changeRoom_bed_typecode.add(b_typecodeList.get(j).text());
												
												List<Element> b_typeList = e.getElementsByTag("bedType");
												changeRoom_bed_type.add(b_typeList.get(j).text());
												
												List<Element> r_planCodeList = e.getElementsByTag("ratePlanCode");
												changeRoom_Rate_plancode.add(r_planCodeList.get(j).text());
												
												List<Element> r_planList = e.getElementsByTag("ratePlan");
												changeRoom_Rate_plan.add(r_planList.get(j).text());
												
												List<Element> r_totalRateList = e.getElementsByTag("totalRate");
												changeRoom_total_rate.add(r_totalRateList.get(j).text().replace(",", ""));
												
											}
										}
									}									
								}
								
								break outerloop;
							}
							
							availableResponseInfo.setRoom_No(room_No);
							availableResponseInfo.setRoom_code(room_code);
							availableResponseInfo.setRoom_typecode(room_typecode);
							availableResponseInfo.setBed_typecode(bed_typecode);
							availableResponseInfo.setRate_plancode(rate_plancode);
							availableResponseInfo.setTotal_rate(totalRate);
							availableResponseInfo.setChangeRoom_room_No(changeRoom_room_No);
							availableResponseInfo.setChangeRoom_total_rate(changeRoom_total_rate);
							availableResponseInfo.setChangeRoom_room_code(changeRoom_room_code);
							availableResponseInfo.setChangeRoom_room_typecode(changeRoom_room_typecode);
							availableResponseInfo.setChangeRoom_room_type(changeRoom_room_type);
							availableResponseInfo.setChangeRoom_bed_typecode(changeRoom_bed_typecode);
							availableResponseInfo.setChangeRoom_bed_type(changeRoom_bed_type);
							availableResponseInfo.setChangeRoom_Rate_plancode(changeRoom_Rate_plancode);
							availableResponseInfo.setChangeRoom_Rate_plan(changeRoom_Rate_plan);
														
							double total = 0;
							DecimalFormat df = new DecimalFormat("#.00"); 
							for (int i = 0; i < totalRate.size(); i++) {					
								total = total + Double.parseDouble(totalRate.get(i));					
							}
							
							availableResponseInfo.setTotal_RoomRate(df.format(total));
																		
						} catch (Exception e) {
							e.toString();
						}
										
					} 
					
					if (!CountRecorded) {
						CountRecorded = true;
						benchmarkCount = hotelcount;
					} else if (hotelcount != benchmarkCount)
						System.out.println("");

				} else {
					
					ErrorCode = doc.getElementsByTag("code").text();
					ErrorDescription = doc.getElementsByTag("description").text();
					availableResponseInfo.setErrorCode(ErrorCode);
					availableResponseInfo.setErrorDiscription(ErrorDescription);
				}

			} catch (Exception e) {
				e.printStackTrace();			
			}
			
			System.out.println(availableResponseInfo.getPageSource());
			responseList.add(availableResponseInfo);
			
					
		}
	}
	
	
	
}
