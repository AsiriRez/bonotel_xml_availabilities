package com.bono.XML_Editors;

/**
 * @author Dulan
 *
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.bono.pojo.SearchScenario;
import com.bono.util.HtmlWriter;


public class AvailabilityXML_Editor {
	
	private String ToFile="Html/HotelAvailabilities_v2016.txt";
 
	public String parser() {

		try {
			File fp = new File("Html/HotelAvailabilities_v2016.txt");
			return getString(fp);

		} catch (Exception ioe) {
			ioe.printStackTrace();
			return null;
		}
	}
	
	public void change_details_HotelCode(SearchScenario searchObject, String occupancy)
	{
		String FrmFile="Html/XMLAvailSearchCity_Hotel-RS12.txt";
		
		String Content=fillCommon(FrmFile, ToFile, occupancy, searchObject);
		Content=Content.replace("C_code", "0");
		Content=Content.replace("H_code", searchObject.getCode());
		System.out.println(Content);
	}
	
	public void change_details_CityCode(SearchScenario searchObject, String occupancy)
	{
		String FrmFile="Html/XMLAvailSearchCity_Hotel-RS12.txt";
	   
		String Content=fillCommon(FrmFile, ToFile, occupancy, searchObject);
		Content=Content.replace("C_code",searchObject.getCode());
		Content=Content.replace("H_code","0");
		System.out.println(Content);
	}
	
	public void change_details_A(SearchScenario searchObject, String occupancy){
		
		String FrmFile="Html/XMLAvailSearch_airport-RS12.txt";
	    
		String Content=fillCommon(FrmFile, ToFile, occupancy, searchObject);
		Content=Content.replace("A_code", searchObject.getCode());
		System.out.println(Content);		
	}
	
	public void change_details_H(SearchScenario searchObject, String occupancy){
		
		String FrmFile="Html/XMLAvailSearch_Group -RS12.txt";
	    
		String Content=fillCommon(FrmFile, ToFile, occupancy, searchObject);
		Content=Content.replace("H_Group",searchObject.getCode());
		System.out.println(Content);
	}
	
	public void change_details_pack(SearchScenario searchObject, String occupancy){
		
		String hotelIds;
		if (searchObject.getSearchBy().equals("HOTEL ID(10)"))
			hotelIds = "10";
		else
			hotelIds = "25";
		
		String FrmFile = "Html/XMLAvailSearch_Hotel_".concat(hotelIds).concat(".txt");

		String Content=fillCommon(FrmFile, ToFile, occupancy, searchObject);
		Content = Content.replace("C_code", "0");
		String[] ids = searchObject.getCode().split(";");

		for (int i = 0; i < ids.length; i++) {
			Content = Content.replace("H_id_".concat(Integer.toString(i + 1)), ids[i]);
		}
		System.out.println(Content);
	}
	
	public void change_details_HotelCodeStayPeriodModifications(SearchScenario searchObject, String occupancy)
	{
		String FrmFile="Html/XMLAvailSearchCity_Hotel-RS12.txt";
		HtmlWriter writer = new HtmlWriter();
		writer.Writetofile(ToFile, false);
		File fp = new File(FrmFile);
		
		String Content = null;
		int diffDays = 0;
		
		try {
			Content = getString(fp);
			Content = Content.replace("user_name", searchObject.getTouroperator());
			Content = Content.replace("Pass_Word", searchObject.getPassword());
			Content = Content.replace("C_code", "0");
			Content = Content.replace("H_code", searchObject.getCode());
			
			SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yy");		
			Date d1 = null;
			Date d2 = null;
			diffDays = 0;
			
			try {
				d1 = format.parse(searchObject.getStayPeriod_FromDate());
				d2 = format.parse(searchObject.getStayPeriod_ToDate());
				long diffD = d2.getTime() - d1.getTime();
				diffDays = (int)(diffD / (24 * 60 * 60 * 1000));			
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Content = Content.replace("Cin_Date", searchObject.getStayPeriod_FromDate());
		Content = Content.replace("Cout_Date", searchObject.getStayPeriod_ToDate());
		Content = Content.replace("non", Integer.toString(diffDays));
		Content = Content.replace("Nor", searchObject.getNoOfRooms());
		Content = Content.replace("TOBEFILLED", occupancy);
		System.out.println(Content);
	}
	
	public String fillCommon(String FrmFile, String ToFile, String occupancy, SearchScenario searchObject) {

		HtmlWriter writer = new HtmlWriter();
		writer.Writetofile(ToFile, false);
		File fp = new File(FrmFile);
		
		try {
			String Content = getString(fp);
			Content = Content.replace("user_name", searchObject.getTouroperator());
			Content = Content.replace("Pass_Word", searchObject.getPassword());
			Content = Content.replace("Cin_Date", searchObject.getCheckInDate());
			Content = Content.replace("Cout_Date", searchObject.getCheckOutDate());
			Content = Content.replace("non", searchObject.getNoOfNights());	
			Content = Content.replace("Nor", searchObject.getNoOfRooms());
			Content = Content.replace("TOBEFILLED", occupancy);
			return Content;
			
		} catch (Exception e) {
			e.printStackTrace();
			return e.toString();
		}
	}
	
	public String getString(File file) 
	{
		try {
			String Code ="";
			 
			BufferedReader br = new BufferedReader(new FileReader(file));

			String Code1 ;
			while (( Code1 = br.readLine()) != null) {
				Code =Code.concat(Code1);
			}
			br.close();
			return Code;
		} catch (Exception e) {
			return null;
		}
	}
	

}