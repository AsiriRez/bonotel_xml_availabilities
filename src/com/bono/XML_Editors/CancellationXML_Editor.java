package com.bono.XML_Editors;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import com.bono.pojo.AvailabilityResponseInfo;
import com.bono.pojo.ReservationResponseInfo;
import com.bono.pojo.SearchScenario;
import com.bono.util.HtmlWriter;

public class CancellationXML_Editor {
	
	private String ToFile="Html/HotelCancellation_v2016.txt";
	 
	public String parser() {

		try {
			File fp = new File("Html/HotelCancellation_v2016.txt");
			return getString(fp);

		} catch (Exception ioe) {
			ioe.printStackTrace();
			return null;
		}
	}
	
	public void getRoomsXMLCancellations(SearchScenario searchObject, ReservationResponseInfo reservationResponseInfo){
		
		try {
			String FrmFile="Html/TestHotelCancellationReq_V.txt";		
			String Content=fillCommon(FrmFile, ToFile, searchObject);
			Content = Content.replace("booking_no", reservationResponseInfo.getBookingNo());
			System.out.println(Content);
			
		} catch (Exception e) {
			e.printStackTrace();
		}				
	}
	
	public String fillCommon(String FrmFile, String ToFile, SearchScenario searchObject){
		
		HtmlWriter writer = new HtmlWriter();
		writer.Writetofile(ToFile, false);
		File fp = new File(FrmFile);
		
		try {
			
			String Content = getString(fp);
			Content = Content.replace("user_name", searchObject.getTouroperator());
			Content = Content.replace("Pass_Word", searchObject.getPassword());
			Content = Content.replace("cancel_Message", "Auto cancellation");
			Content = Content.replace("cancel_Notes", "Cancellation Notes");
						
			return Content;
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			return e.toString();
		}
		
	}
	
	public String getString(File file) {
		try {
			String Code ="";
			 
			BufferedReader br = new BufferedReader(new FileReader(file));

			String Code1 ;
			while (( Code1 = br.readLine()) != null) {
				Code =Code.concat(Code1);
			}
			br.close();
			return Code;
		} catch (Exception e) {
			return null;
		}
	}
}
