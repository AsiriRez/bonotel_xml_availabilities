package com.bono.XML_Editors;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.bono.handlers.HttpRequestHandler;
import com.bono.handlers.OccupancyHandler;
import com.bono.handlers.RoomDetailHandler;
import com.bono.pojo.AvailabilityResponseInfo;
import com.bono.pojo.ReservationResponseInfo;
import com.bono.pojo.SearchScenario;
import com.bono.util.HtmlWriter;

public class ModificationXML_Editor {
	
	private String[] adultsList;
	private String[] childList;
	private String ToFile="Html/HotelModification_v2016.txt";
	private String roomData_GuestDetails = "<roomData><roomNo>Room_No</roomNo><roomResNo>RoomRes_No</roomResNo><noOfAdults>R_AdultCount</noOfAdults><noOfChildren>R_ChildCount</noOfChildren><occupancy>RoomOccupancyDetails</occupancy></roomData>";
	private String roomData = "<roomData><roomNo>Room_No</roomNo><roomCode>R_room_code</roomCode><roomTypeCode>R_room_typecode</roomTypeCode><bedTypeCode>R_bed_typecode</bedTypeCode><ratePlanCode>R_Rate_plancode</ratePlanCode><noOfAdults>R_AdultCount</noOfAdults><noOfChildren>R_ChildCount</noOfChildren><occupancy>RoomOccupancyDetails</occupancy></roomData>";
	private String roomData_ChangeRoom = "<roomData><roomNo>Room_No</roomNo><roomResNo>RoomRes_No</roomResNo><roomCode>R_room_code</roomCode><roomTypeCode>R_room_typecode</roomTypeCode><bedTypeCode>R_bed_typecode</bedTypeCode><ratePlanCode>R_Rate_plancode</ratePlanCode><noOfAdults>R_AdultCount</noOfAdults><noOfChildren>R_ChildCount</noOfChildren><occupancy>RoomOccupancyDetails</occupancy></roomData>";
	private HttpRequestHandler requestHandler;	
	
	public ModificationXML_Editor(HttpRequestHandler requestHandle){
		this.requestHandler = requestHandle;
	}
	
	public String parser() {

		try {
			File fp = new File("Html/HotelModification_v2016.txt");
			return getString(fp);

		} catch (Exception ioe) {
			ioe.printStackTrace();
			return null;
		}
	}
	
	public void getRoomsXMLModifications(SearchScenario searchObject, ReservationResponseInfo reservationResponseInfo, ArrayList<AvailabilityResponseInfo> availableResponseInfoList){
		
		RoomDetailHandler roomHandler = new RoomDetailHandler();
		ArrayList<String> roomDataList = new ArrayList<String>();
		adultsList = searchObject.getRoomAD().split("/");
		childList = searchObject.getRoomCH().split("/");	
		
		List<String> occupancyList = new ArrayList<String>();
		occupancyList = roomHandler.getOccupancy(searchObject);	
		AvailabilityResponseInfo availabilityResObj = availableResponseInfoList.get(0);
		
		try {
			
			//GuestDetails
			if (searchObject.getModificationType().equalsIgnoreCase("GuestDetails")) {
				
				String FrmFile="Html/XMLModify_GuestDetails.txt";		
				String Content=fillCommonDetails(FrmFile, ToFile, searchObject);
				
				Content = Content.replace("booking_no", reservationResponseInfo.getBookingNo());
				Content = Content.replace("noOf_Rooms", searchObject.getNoOfRooms());
				
				for (int i = 1; i <= Integer.parseInt(searchObject.getNoOfRooms()); i++) {
					
					String originalString = roomData_GuestDetails;
					originalString=originalString.replace("Room_No", ""+i+"");
					originalString=originalString.replace("RoomRes_No", ""+reservationResponseInfo.getRoomResNo().get(i-1)+"");
					originalString=originalString.replace("R_AdultCount", adultsList[i-1]);
					originalString=originalString.replace("R_ChildCount", childList[i-1]);				
					originalString=originalString.replace("RoomOccupancyDetails", occupancyList.get(i-1));
					roomDataList.add(originalString);
				}
				
				String newRoomData = "";
				for (int i = 0; i < roomDataList.size(); i++) {
					newRoomData = newRoomData.concat(roomDataList.get(i));
				}
				
				Content = Content.replace("Room_Information", newRoomData);						
				System.out.println(Content);
			}
			
			//AddRoom
			if (searchObject.getModificationType().equalsIgnoreCase("AddRoom")) {
				
				String FrmFile="Html/XMLModify_AddRoom.txt";
				String AdultStream = "<guest><title>Mr</title><firstName>newAdultName</firstName><lastName>TEST</lastName></guest>";
				String Content=fillCommonDetails(FrmFile, ToFile, searchObject);
				
				Content = Content.replace("booking_no", reservationResponseInfo.getBookingNo());
				Content = Content.replace("noOf_Rooms", "1");
						
				int newRoom = Integer.parseInt(searchObject.getNoOfRooms()) +1;
				
				String originalString = roomData;
				originalString=originalString.replace("Room_No", ""+newRoom+"");
				originalString=originalString.replace("R_room_code", availabilityResObj.getRoom_code().get(0));
				originalString=originalString.replace("R_room_typecode", availabilityResObj.getRoom_typecode().get(0));
				originalString=originalString.replace("R_bed_typecode", availabilityResObj.getBed_typecode().get(0));
				originalString=originalString.replace("R_Rate_plancode", availabilityResObj.getRate_plancode().get(0));
				originalString=originalString.replace("R_AdultCount", "1");		
				originalString=originalString.replace("R_ChildCount", "0");	
				originalString=originalString.replace("RoomOccupancyDetails", AdultStream);
				
				Content = Content.replace("Room_Information", originalString);	
				
				Content = Content.replace("hotel_id", availabilityResObj.getHotel_id());
				Content = Content.replace("total_rate", availabilityResObj.getTotal_rate().get(0));
			    Content = Content.replace("total_tax", "0.00");
			    System.out.println(Content);				
			}
			
			//ChangeNotes
			if (searchObject.getModificationType().equalsIgnoreCase("ChangeNotes")) {
			
				String FrmFile="Html/XMLModify_ChangeNotes.txt";
				String Content=fillCommonDetails(FrmFile, ToFile, searchObject);
				
				Content = Content.replace("booking_no", reservationResponseInfo.getBookingNo());
				Content = Content.replace("Hotel_Comment", "new Hotel room modifications hotel comment");	
				System.out.println(Content);
			}
			
			//ChangeRoom
			if (searchObject.getModificationType().equalsIgnoreCase("ChangeRoom")) {
				
				String FrmFile="Html/XMLModify_ChangeRoom.txt";
				String Content=fillCommonDetails(FrmFile, ToFile, searchObject);
				
				Content = Content.replace("booking_no", reservationResponseInfo.getBookingNo());
				Content = Content.replace("noOf_Rooms", "1");
				
				String roomCodeFirst = availabilityResObj.getRoom_code().get(0);
				
				for (int i = 0; i < availabilityResObj.getChangeRoom_room_No().size(); i++) {
					
					String originalString = roomData_ChangeRoom;
					
					if (!(roomCodeFirst.equals(availabilityResObj.getChangeRoom_room_code().get(i)))) {
												
						originalString=originalString.replace("Room_No", "1");
						originalString=originalString.replace("RoomRes_No", reservationResponseInfo.getRoomResNo().get(0));
						originalString=originalString.replace("R_room_code", availabilityResObj.getChangeRoom_room_code().get(i));
						originalString=originalString.replace("R_room_typecode", availabilityResObj.getChangeRoom_room_typecode().get(i));
						originalString=originalString.replace("R_bed_typecode", availabilityResObj.getChangeRoom_bed_typecode().get(i));
						originalString=originalString.replace("R_Rate_plancode", availabilityResObj.getChangeRoom_Rate_plancode().get(i));
						originalString=originalString.replace("R_AdultCount", adultsList[0]);		
						originalString=originalString.replace("R_ChildCount", childList[0]);	
						originalString=originalString.replace("RoomOccupancyDetails", searchObject.getStayPeriod_occupancyList().get(0));
						
						Content = Content.replace("Room_Information", originalString);
						Content = Content.replace("total_rate", availabilityResObj.getChangeRoom_total_rate().get(i));
						
						break;
					}				
				}
				
				Content = Content.replace("hotel_id", availabilityResObj.getHotel_id());
				Content = Content.replace("total_tax", "0.00");
				System.out.println(Content);
			}
			
			//StayPeriod
			if (searchObject.getModificationType().equalsIgnoreCase("StayPeriod")) {
				
				AvailabilityXML_Editor availableXMLEdit = new AvailabilityXML_Editor(); 
				OccupancyHandler occupancyHandler =new OccupancyHandler(searchObject);
				String occupancy = occupancyHandler.getOccupancy();
				availableXMLEdit.change_details_HotelCodeStayPeriodModifications(searchObject, occupancy);
				
				String Value = "xml=".concat(availableXMLEdit.parser()).replace("<city>0</city>","");
				String Response = null;
				String newStayPeriodTotal = null;
				
				try {
					Response = requestHandler.sendPost(Value.replace(" ", ""), availabilityResObj.getAvailability_URL());
					Document doc = Jsoup.parse(Response);
					System.out.println("Stay_Period Availability Response : " + Response);
					
					String status = null;
					String genHotelCode = searchObject.getCode();
					ArrayList<String> totalRate = new ArrayList<String>();
					status = doc.getElementsByTag("availabilityresponse").first().attr("status");
					
					if (status.equals("Y")) {
						
						List<Element> hotelElements = doc.getElementsByTag("hotel");
						
						outerloop : for (Element e : hotelElements){
							
							String xmlHotelCode = e.getElementsByTag("hotelCode").text();
							
							if (xmlHotelCode.equals(genHotelCode)) {
								
								List<Element> roomElement = e.getElementsByTag("roomNo");
								
								for (int i = 1; i <= Integer.parseInt(searchObject.getNoOfRooms()); i++) {							
									innerloop : for (int j = 0; j < roomElement.size(); j++) {
										
										if(i == Integer.parseInt(roomElement.get(j).text())){
											
											List<Element> r_codeList = e.getElementsByTag("roomCode");
											List<Element> r_typecodeList = e.getElementsByTag("roomTypeCode");
											List<Element> b_typecodeList = e.getElementsByTag("bedTypeCode");
											List<Element> r_planCodeList = e.getElementsByTag("ratePlanCode");
											List<Element> r_totalRateList = e.getElementsByTag("totalRate");
											
											if(availabilityResObj.getRoom_code().get(i-1).equals(r_codeList.get(j).text()) && 
													availabilityResObj.getRoom_typecode().get(i-1).equals(r_typecodeList.get(j).text()) &&
														availabilityResObj.getBed_typecode().get(i-1).equals(b_typecodeList.get(j).text()) &&
															availabilityResObj.getRate_plancode().get(i-1).equals(r_planCodeList.get(j).text())){
												
												totalRate.add(r_totalRateList.get(j).text().replace(",", ""));											
												break innerloop;
											}				
										}
									}														
								}								
							}
							
							break outerloop;
						}
						
						double total = 0;
						DecimalFormat df = new DecimalFormat("#.00"); 
						
						if (!(totalRate == null)) {
							for (int i = 0; i < totalRate.size(); i++) {					
								total = total + Double.parseDouble(totalRate.get(i));					
							}
							newStayPeriodTotal = df.format(total);
							
						} else {
							newStayPeriodTotal = df.format(total);
						}
						
					}else{
						String errorCode = doc.getElementsByTag("code").text();
						String errorDescription = doc.getElementsByTag("description").text();
						System.out.println("Error Code :- " +errorCode+ "Error Description :" +errorDescription+"");
					}
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				//Saving file
				String FrmFile="Html/XMLModify_StayPeriod.txt";
				
				HtmlWriter writer = new HtmlWriter();
				writer.Writetofile(ToFile, false);
				File fp = new File(FrmFile);
				
				try {				
					String Content = getString(fp);
					Content = Content.replace("user_name", searchObject.getTouroperator());
					Content = Content.replace("Pass_Word", searchObject.getPassword());
					Content = Content.replace("booking_no", reservationResponseInfo.getBookingNo());
					Content = Content.replace("check_in", searchObject.getStayPeriod_FromDate());
					Content = Content.replace("check_out", searchObject.getStayPeriod_ToDate());
					
					SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yy");		
					Date d1 = null;
					Date d2 = null;
					int diffDays = 0;
					
					try {
						d1 = format.parse(searchObject.getStayPeriod_FromDate());
						d2 = format.parse(searchObject.getStayPeriod_ToDate());
						long diffD = d2.getTime() - d1.getTime();
						diffDays = (int)(diffD / (24 * 60 * 60 * 1000));			
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					Content = Content.replace("noOf_Rooms", searchObject.getNoOfRooms());
					Content = Content.replace("non", Integer.toString(diffDays));
					Content = Content.replace("hotel_id", availabilityResObj.getHotel_id());
					Content = Content.replace("total_rate", newStayPeriodTotal);
					Content = Content.replace("total_tax", "0.00");
					
					for (int i = 1; i <= Integer.parseInt(searchObject.getNoOfRooms()); i++) {
						
						String originalString = roomData_ChangeRoom;
						originalString=originalString.replace("Room_No", ""+i+"");
						originalString=originalString.replace("RoomRes_No", reservationResponseInfo.getRoomResNo().get(i-1));
						originalString=originalString.replace("R_room_code", availabilityResObj.getRoom_code().get(i-1));
						originalString=originalString.replace("R_room_typecode", availabilityResObj.getRoom_typecode().get(i-1));
						originalString=originalString.replace("R_bed_typecode", availabilityResObj.getBed_typecode().get(i-1));
						originalString=originalString.replace("R_Rate_plancode", availabilityResObj.getRate_plancode().get(i-1));
						originalString=originalString.replace("R_AdultCount", adultsList[i-1]);
						originalString=originalString.replace("R_ChildCount", childList[i-1]);				
						originalString=originalString.replace("RoomOccupancyDetails", searchObject.getStayPeriod_occupancyList().get(i-1));
						roomDataList.add(originalString);
					}
					
					String newRoomData = "";
					for (int i = 0; i < roomDataList.size(); i++) {
						newRoomData = newRoomData.concat(roomDataList.get(i));
					}
					
					Content = Content.replace("Room_Information", newRoomData);
					Content = Content.replace("Customer_Comments", "Customer room modifications comment");	
					System.out.println(Content);
				
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println(e.getMessage());
				}
			}
			
			if (searchObject.getModificationType().equalsIgnoreCase("ToOrderNo")) {
				
				String FrmFile="Html/XMLModify_ToOrderNo.txt";
				String Content=fillCommonDetails(FrmFile, ToFile, searchObject);
				
				Content = Content.replace("booking_no", reservationResponseInfo.getBookingNo());
				Content = Content.replace("newTONumber", "123456789");				
				System.out.println(Content);
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}				
	}
	
	public String fillCommonDetails(String FrmFile, String ToFile, SearchScenario searchObject){
		
		HtmlWriter writer = new HtmlWriter();
		writer.Writetofile(ToFile, false);
		File fp = new File(FrmFile);
		
		try {			
			String Content = getString(fp);
			Content = Content.replace("user_name", searchObject.getTouroperator());
			Content = Content.replace("Pass_Word", searchObject.getPassword());
			Content = Content.replace("check_in", searchObject.getCheckInDate() );
			Content = Content.replace("check_out", searchObject.getCheckOutDate());
			Content = Content.replace("non", searchObject.getNoOfNights());
			Content = Content.replace("Customer_Comments", "Customer room modifications comment");	
						
			return Content;			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			return e.toString();
		}
		
	}
	
	public String getString(File file) {
		try {
			String Code ="";
			 
			BufferedReader br = new BufferedReader(new FileReader(file));

			String Code1 ;
			while (( Code1 = br.readLine()) != null) {
				Code =Code.concat(Code1);
			}
			br.close();
			return Code;
		} catch (Exception e) {
			return null;
		}
	}

}
