package com.bono.XML_Editors;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import com.bono.handlers.RoomDetailHandler;
import com.bono.pojo.AvailabilityResponseInfo;
import com.bono.pojo.SearchScenario;
import com.bono.util.HtmlWriter;

public class ReservationXML_Editor {
	
	private String[] adultsList;
	private String[] childList;
	private ArrayList<AvailabilityResponseInfo> availableResponseInfoList;
	private String ToFile="Html/HotelReservation_v2016.txt";
	private String roomData = "<roomData><roomNo>Room_No</roomNo><roomCode>R_room_code</roomCode><roomTypeCode>R_room_typecode</roomTypeCode><bedTypeCode>R_bed_typecode</bedTypeCode><ratePlanCode>R_Rate_plancode</ratePlanCode><noOfAdults>R_AdultCount</noOfAdults><noOfChildren>R_ChildCount</noOfChildren><occupancy>RoomOccupancyDetails</occupancy></roomData>";
	
	public ReservationXML_Editor(ArrayList<AvailabilityResponseInfo> availableResponseList){
		this.availableResponseInfoList = availableResponseList;
	}
	
	public String parser() {

		try {
			File fp = new File("Html/HotelReservation_v2016.txt");
			return getString(fp);

		} catch (Exception ioe) {
			ioe.printStackTrace();
			return null;
		}
	}
	
	public void getRoomDataInfoLoaded(SearchScenario searchObject){
		
		RoomDetailHandler roomHandler = new RoomDetailHandler();
		ArrayList<String> roomDataList = new ArrayList<String>();
		AvailabilityResponseInfo availabilityResObj = availableResponseInfoList.get(0);
		adultsList = searchObject.getRoomAD().split("/");
		childList = searchObject.getRoomCH().split("/");	
		
		String FrmFile="Html/TestHotelReservationReq_V.txt";	
		String Content = fillCommon(FrmFile, ToFile, searchObject);
		
		List<String> occupancyList = new ArrayList<String>();
		occupancyList = roomHandler.getOccupancy(searchObject);	
		searchObject.setStayPeriod_occupancyList(occupancyList);
		
		for (int i = 1; i <= Integer.parseInt(searchObject.getNoOfRooms()); i++) {
			
			String originalString = roomData;
			originalString=originalString.replace("Room_No", ""+i+"");
			originalString=originalString.replace("R_room_code", availabilityResObj.getRoom_code().get(i-1));
			originalString=originalString.replace("R_room_typecode", availabilityResObj.getRoom_typecode().get(i-1));
			originalString=originalString.replace("R_bed_typecode", availabilityResObj.getBed_typecode().get(i-1));
			originalString=originalString.replace("R_Rate_plancode", availabilityResObj.getRate_plancode().get(i-1));
			originalString=originalString.replace("R_AdultCount", adultsList[i-1]);
			originalString=originalString.replace("R_ChildCount", childList[i-1]);				
			originalString=originalString.replace("RoomOccupancyDetails", occupancyList.get(i-1));
			roomDataList.add(originalString);
		}
		
		String newRoomData = "";
		for (int i = 0; i < roomDataList.size(); i++) {
			newRoomData = newRoomData.concat(roomDataList.get(i));
		}
		
		Content = Content.replace("Room_Information", newRoomData);
		System.out.println(Content);
	}
	
	
	public String fillCommon(String FrmFile, String ToFile, SearchScenario searchObject){
		
		HtmlWriter writer = new HtmlWriter();
		writer.Writetofile(ToFile, false);
		File fp = new File(FrmFile);
		AvailabilityResponseInfo availabilityResObj = availableResponseInfoList.get(0);
		
		try {
			
			String Content = getString(fp);
			Content = Content.replace("user_name", searchObject.getTouroperator());
			Content = Content.replace("Pass_Word", searchObject.getPassword());
			Content=Content.replace("check_in", searchObject.getCheckInDate() );
			Content=Content.replace("check_out", searchObject.getCheckOutDate());
			Content=Content.replace("nor", searchObject.getNoOfRooms());
			Content=Content.replace("non", searchObject.getNoOfNights());
			
			Content=Content.replace("hotel_id", availabilityResObj.getHotel_id());
			Content=Content.replace("total_rate", availabilityResObj.getTotal_RoomRate());
		    Content=Content.replace("total_tax", "0.00");
			
			return Content;
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			return e.toString();
		}
		
	}
	
	
	public String getString(File file) {
		
		try {
			String Code ="";
			 
			BufferedReader br = new BufferedReader(new FileReader(file));

			String Code1 ;
			while (( Code1 = br.readLine()) != null) {
				Code =Code.concat(Code1);
			}
			br.close();
			return Code;
		} catch (Exception e) {
			return null;
		}
	}

}
