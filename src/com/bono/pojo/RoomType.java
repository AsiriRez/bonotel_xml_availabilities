package com.bono.pojo;

import java.util.ArrayList;

public class RoomType {
	
	private String   HotelName           = "";
	private String   RoomType            = "";
	private String   BedType             = "";
	private String   RateType            = "";
	private String   Adults              = "";
	private String   AditionalAdults     = "";
	private String   Child               = "";
	private String   ARate               = "";
	private String   AARate              = "";
	private String   CRate               = "";
	private String   Aprofit             = "";
	private String   AAprofit            = "";
	private String   CProfit             = "";
	private int      ApplyIflessThan;
	private String   CancellationType    = "";
	private String   StdFee              = "";
	private String   NoShowFee           = "";
	private String   StarRating          = "";
	private String   SearchAdults               = "";
	private String   SearchAditionalAdults      = "";
	private String   SearchChilds               = "";
	private boolean  PromotionApplied                     ;
	private String   PromoCode                  = "";

//	private RateClass      RoomRateObject       = null;

	private String            RoomLable           = "";
    private ArrayList<String>  DailyRates         = null;
	private String            TotalRate           = "";
	private String            Currency            = "";
	
	//TODO:Implement Daily Rates here
	

	public void setApplyIflessThan(int applyIflessThan) {
		ApplyIflessThan = applyIflessThan;
	}
	public boolean isPromotionApplied() {
		return PromotionApplied;
	}
	public void setPromotionApplied(boolean promotionApplied) {
		PromotionApplied = promotionApplied;
	}
	public String getPromoCode() {
		return PromoCode;
	}
	public void setPromoCode(String promoCode) {
		PromoCode = promoCode;
	}

	public String getRoomLable() {
		return RoomLable;
	}
	public void setRoomLable(String roomLable) {
		RoomLable = roomLable;
	}
	public ArrayList<String> getDailyRates() {
		return DailyRates;
	}
	public void setDailyRates(ArrayList<String> dailyRates) {
		DailyRates = dailyRates;
	}
	public String getTotalRate() {
		return TotalRate;
	}
	public void setTotalRate(String totalRate) {
		TotalRate = totalRate;
	}
	public String getCurrency() {
		return Currency;
	}
	public void setCurrency(String currency) {
		Currency = currency;
	}
	public String getSearchAdults() {
		return SearchAdults;
	}
	public void setSearchAdults(String searchAdults) {
		SearchAdults = searchAdults;
	}
	public String getSearchAditionalAdults() {
		return SearchAditionalAdults;
	}
	public void setSearchAditionalAdults(String searchAditionalAdults) {
		SearchAditionalAdults = searchAditionalAdults;
	}
	public String getSearchChilds() {
		return SearchChilds;
	}
	public void setSearchChilds(String searchChilds) {
		SearchChilds = searchChilds;
	}
	public String getStarRating() {
		return StarRating;
	}
	public void setStarRating(String starRating) {
		StarRating = starRating;
	}
	public String getHotelName() {
		return HotelName;
	}
	public void setHotelName(String hotelName) {
		HotelName = hotelName;
	}
	public String getRoomType() {
		return RoomType;
	}
	public void setRoomType(String roomType) {
		RoomType = roomType;
	}
	public String getBedType() {
		return BedType;
	}
	public void setBedType(String bedType) {
		BedType = bedType;
	}
	public String getRateType() {
		return RateType;
	}
	public void setRateType(String rateType) {
		RateType = rateType;
	}
	public String getAdults() {
		return Adults;
	}
	public void setAdults(String adults) {
		Adults = adults;
	}
	public String getAditionalAdults() {
		return AditionalAdults;
	}
	public void setAditionalAdults(String aditionalAdults) {
		AditionalAdults = aditionalAdults;
	}
	public String getChild() {
		return Child;
	}
	public void setChild(String child) {
		Child = child;
	}
	public String getARate() {
		return ARate;
	}
	public void setARate(String aRate) {
		ARate = aRate;
	}
	public String getAARate() {
		return AARate;
	}
	public void setAARate(String aARate) {
		AARate = aARate;
	}
	public String getCRate() {
		return CRate;
	}
	public void setCRate(String cRate) {
		CRate = cRate;
	}
	public String getAprofit() {
		return Aprofit;
	}
	public void setAprofit(String aprofit) {
		Aprofit = aprofit;
	}
	public String getAAprofit() {
		return AAprofit;
	}
	public void setAAprofit(String aAprofit) {
		AAprofit = aAprofit;
	}
	public String getCProfit() {
		return CProfit;
	}
	public void setCProfit(String cProfit) {
		CProfit = cProfit;
	}
	public int getApplyIflessThan() {
		return ApplyIflessThan;
	}
	public void setApplyIflessThan(String applyIflessThan) {
		ApplyIflessThan = Integer.parseInt(applyIflessThan);
	}
	public String getCancellationType() {
		return CancellationType;
	}
	public void setCancellationType(String cancellationType) {
		CancellationType = cancellationType;
	}
	public String getStdFee() {
		return StdFee;
	}
	public void setStdFee(String stdFee) {
		StdFee = stdFee;
	}
	public String getNoShowFee() {
		return NoShowFee;
	}
	public void setNoShowFee(String noShowFee) {
		NoShowFee = noShowFee;
	}

		 

}
