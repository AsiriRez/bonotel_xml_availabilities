package com.bono.pojo;


import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import types.BookingType;



public class SearchType {
	
	private String   SearchIndex             = "";
	private String   CountryOFResidance      = "";
	private String   Location                = "";
	private String   HiddenLoc               = "";
	//private types.BookingType   BookingType  =BookingType.NONE;
	private String   Nights                  = "";
	private String   Rooms                   = "";
	private String[]   Adults                = null;
	private String[]   Children              = null;
//	private String   Children                 = "";
	private Map<Integer, String[]> ChildAges = null;
	private ArrayList<String>   RoomTypes    = null;
	private String   Description             = "";
	private String   Expected                = "";
	private String   SearchCurrency          = "";
	private String   BaseCurrency            = "";
	private String   ArrivalDate             = "";
	private String   CheckOutDate            = "";
	private   Map<String, String>   DefaultRooms                                              = new TreeMap<String, String>();
	private   Map<String, String>   SelectedRooms                                             = new TreeMap<String, String>();
	private boolean HotelAvailability        ;
    @SuppressWarnings("unused")
	private BookingType bookingType = null;
	private String   HotelName             = "";
	private String   BookingDate           = "";
	private String   Availability          = "";
	private String   TotalRate             = "";
	private String   BookinDateType        = "";
    //private Hotel                              ValidationHotel                      = new Hotel();
	private Map<String, ArrayList<RoomType>>   ResultsRoomMap                       = new HashMap<String, ArrayList<RoomType>>();
	private CartDetails                        Cart                                 = new CartDetails();
	private CancellationPolicy                 policy                               = new CancellationPolicy();
    private CancellationPolicy                 CartPolicy                           = new CancellationPolicy();
	private ArrayList<RoomType>                SelectedRoomObj                      = new ArrayList<RoomType>();
    private boolean  ScenarioExcecutionState                                        = false;
    private String   BookingChannel                                                 = "Call Center";
    private String Pay_CustomerNote                = "Automation Customer Note Automation Customer Note Automation Customer Note Automation Customer Note Automation Customer Note Automation Customer Note ";
	private String Pay_InternalNote                = "Auto Internal Note Auto Internal Note Auto Internal Note Auto Internal Note Auto Internal Note Auto Internal Note Auto Internal Note Auto Internal Note Auto Internal Note";
	private String Pay_HotelNote                   = "Auto Hotel Note Auto Hotel Note Auto Hotel Note Auto Hotel Note Auto Hotel Note Auto Hotel Note Auto Hotel Note Auto Hotel Note Auto Hotel Note Auto Hotel Note ";
	private Boolean isCopyMails                    = true;
	private String  PaymentType                    = "-";
	private String  CardType                       = "-";
	private String  OfflineMethod                  = "-";
	private String  PaymentRef                     = "-";
	private boolean isOptionalSupplementarySelected= false;
	private boolean BestRateApplied                = false;
	private boolean DealOfTheDayApplied            = false;
	
	
	
	
	
	


	public boolean isBestRateApplied() {
		return BestRateApplied;
	}

	public void setBestRateApplied(String bestRateApplied) {
		if (bestRateApplied.equalsIgnoreCase("yes"))
			BestRateApplied = true;
		else
			BestRateApplied = false;
	}

	public boolean isDealOfTheDayApplied() {
		return DealOfTheDayApplied;
	}

	public void setDealOfTheDayApplied(String dealOfTheDayApplied) {
		if (dealOfTheDayApplied.equalsIgnoreCase("yes"))
			DealOfTheDayApplied = true;
		else
			DealOfTheDayApplied = false;
	}

	public boolean isOptionalSupplementarySelected() {
		return isOptionalSupplementarySelected;
	}

	public void setOptionalSupplementarySelected(
			boolean isOptionalSupplementarySelected) {
		this.isOptionalSupplementarySelected = isOptionalSupplementarySelected;
	}

	public ArrayList<RoomType> getSelectedRoomObj() {
		return SelectedRoomObj;
	}

	public void addToSelectedRoomObj(RoomType selectedRoom) {
		SelectedRoomObj.add(selectedRoom);
	}

	public String getBaseCurrency() {
		return BaseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		BaseCurrency = baseCurrency;
	}

	public String getBookingChannel() {
		return BookingChannel;
	}

	public void setBookingChannel(String bookingChannel) {
		BookingChannel = bookingChannel;
	}

	public boolean isScenarioExcecutionState() {
		return ScenarioExcecutionState;
	}

	public void setScenarioExcecutionState(String scenarioExcecutionState) {
		if(scenarioExcecutionState.trim().equalsIgnoreCase("True"))
		ScenarioExcecutionState = true;
		else if(scenarioExcecutionState.trim().equalsIgnoreCase("false"))
		ScenarioExcecutionState = false;
	}
	public Map<String, String> getSelectedRooms() {
		return SelectedRooms;
	}

	public void addToSelectedRooms(String roomnum, String RoomString) {
		SelectedRooms.put(roomnum, RoomString);
	}
	public Map<String, String> getDefaultRooms() {
		return DefaultRooms;
	}

	public void addToDefaultRooms(String roomnum, String RoomString) {
		DefaultRooms.put(roomnum, RoomString);
	}
	public String getPaymentType() {
		return PaymentType;
	}

	public void setPaymentType(String paymentType) {
		PaymentType = paymentType;
	}

	public String getCardType() {
		return CardType;
	}

	public void setCardType(String cardType) {
		CardType = cardType;
	}

	public String getOfflineMethod() {
		return OfflineMethod;
	}

	public void setOfflineMethod(String offlineMethod) {
		OfflineMethod = offlineMethod;
	}

	public String getPaymentRef() {
		return PaymentRef;
	}

	public void setPaymentRef(String paymentRef) {
		PaymentRef = paymentRef;
	}

	public ArrayList<String> getRoomTypes() {
		return RoomTypes;
	}

	public void setRoomTypes(ArrayList<String> roomTypes) {
		RoomTypes = roomTypes;
	}

	public String getPay_CustomerNote() {
		return Pay_CustomerNote;
	}

	public void setPay_CustomerNote(String pay_CustomerNote) {
		Pay_CustomerNote = pay_CustomerNote;
	}

	public String getPay_InternalNote() {
		return Pay_InternalNote;
	}

	public void setPay_InternalNote(String pay_InternalNote) {
		Pay_InternalNote = pay_InternalNote;
	}

	public String getPay_HotelNote() {
		return Pay_HotelNote;
	}

	public void setPay_HotelNote(String pay_HotelNote) {
		Pay_HotelNote = pay_HotelNote;
	}

	public Boolean getIsCopyMails() {
		return isCopyMails;
	}

	public void setIsCopyMails(Boolean isCopyMails) {
		this.isCopyMails = isCopyMails;
	}

	public void setAdults(String[] adults) {
		Adults = adults;
	}

	public void setChildren(String[] children) {
		Children = children;
	}

	public void setChildAges(Map<Integer, String[]> childAges) {
		ChildAges = childAges;
	}

	public void setCheckOutDate(String checkOutDate) {
		CheckOutDate = checkOutDate;
	}

	public void setResultsRoomMap(Map<String, ArrayList<RoomType>> resultsRoomMap) {
		ResultsRoomMap = resultsRoomMap;
	}

	public String getSearchIndex() {
		return SearchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		SearchIndex = searchIndex;
	}

	/*public Hotel getValidationHotel() {
		return ValidationHotel;
	}

	public void setValidationHotel(Hotel validationHotel) {
		ValidationHotel = validationHotel;
	}*/

	public Map<String, ArrayList<RoomType>> getResultsRoomMap() {
		return ResultsRoomMap;
	}

	public void addToResultsRoomMap(ArrayList<RoomType> RoomList, String Room) {
		ResultsRoomMap.put(Room, RoomList);
	}

	public CartDetails getCart() {
		return Cart;
	}
	public void setCart(CartDetails cart) {
		Cart = cart;
	}
	public CancellationPolicy getPolicy() {
		return policy;
	}
	public void setPolicy(CancellationPolicy policy) {
		this.policy = policy;
	}


	public String getBookinDateType() {
		return BookinDateType;
	}

	public void setBookinDateType(String bookinDateType) {
		BookinDateType = bookinDateType;
	}

	public String getBookingDate() {
		return BookingDate;
	}

	public void setBookingDate(String bookingDate) {
		
		if(!bookingDate.trim().equalsIgnoreCase("-")){
		  Calendar cal = Calendar.getInstance();
		  SimpleDateFormat sdf  = new SimpleDateFormat("dd-MMM-yyyy"); 
		  SimpleDateFormat sdf2 = new SimpleDateFormat("MM/dd/yyyy");
		  Date Checkin   = null;
		  
		  try {
			   Checkin         = sdf.parse(bookingDate.trim());
			   cal.setTime(Checkin);
			   this.ArrivalDate  = sdf2.format(cal.getTime());
			   cal.add(Calendar.DATE, Integer.parseInt(this.Nights));
			   this.CheckOutDate = sdf2.format(cal.getTime());
		} catch (Exception e) {
			System.out.println("Pharse Error: "+bookingDate+" "+e.toString());
		}
		}
		else
		{
			BookingDate = "-";	
		}
		
	}

	public String getAvailability() {
		return Availability;
	}

	public void setAvailability(String availability) {
		Availability = availability;
	}

	public String getTotalRate() {
		return TotalRate;
	}

	public void setTotalRate(String totalRate) {
		TotalRate = totalRate;
	}

	public void setHotelAvailability(boolean hotelAvailability) {
		HotelAvailability = hotelAvailability;
	}

	public String getHotelName() {
		return HotelName;
	}

	public void setHotelName(String hotelName) {
		HotelName = hotelName;
	}

	public void setBookingType(BookingType bookingType) {
		this.bookingType = bookingType;
	}

	public boolean isHotelAvailability() {
		return HotelAvailability;
	}

	public void setHotelAvailability(String hotelAvailability) {
		if(hotelAvailability.equalsIgnoreCase("Available"))
			HotelAvailability = true;
		else
			HotelAvailability = false;	
	}

	public String getCheckOutDate() {
		return CheckOutDate;
	}

	public void setSearchCurrency(String Currency) {
		SearchCurrency = Currency;
	}
	public String getSearchCurrency() {
	    return SearchCurrency;
	}
	
	public String getCountryOFResidance() {
		return CountryOFResidance;
	}
	
	public void setCountryOFResidance(String countryOFResidance) {
		CountryOFResidance = countryOFResidance;
	}
	
	public String getLocation() {
		return Location;
	}
	
	public void setLocation(String location) {
		Location = location;
	}
	
	public String getHiddenLoc() {
		return HiddenLoc;
	}
	
	public void setHiddenLoc(String hiddenLoc) {
		HiddenLoc = hiddenLoc;
	}
	
	public BookingType getBookingType() {
		return bookingType;
	}
	
	/*public void setBookingType(String bookingType) {
		bookingType = BookingType.getBookingType(bookingType.trim());
		
		if(this.BookingDate.equalsIgnoreCase("-")){
			
			if(BookingType.equals("OutSideCancellation"))
			 {
		        ArrivalDate  =  Calender.getDate(Calendar.MONTH , 1, "MM/dd/yyyy");
		        CheckOutDate =  Calender.getDate(1, Integer.parseInt(Nights),  "MM/dd/yyyy","");
			 }
			 else
			 {
			    ArrivalDate  =  Calender.getDate(Calendar.DATE  , 1, "MM/dd/yyyy");
			    CheckOutDate =  Calender.getDate(Calendar.DATE  , 1+Integer.parseInt(Nights), "MM/dd/yyyy");
			 }
		}
	     
	}*/
	
	public String getNights() {
		return Nights;
	}
	
	public void setNights(String nights) {
		Nights = nights;
	}
	
	public String getRooms() {
		return Rooms;
	}
	
	public void setRooms(String rooms) {
		Rooms = rooms;
	}
	
	public String[] getAdults() {
		return Adults;
	}
	
	public void setAdults(String adults) {
		Adults = adults.split(";");
	}
	
	public String[] getChildren() {
		return Children;
	}
	
	public void setChildren(String children) {
		Children = children.split(";");
	}
	
	public Map<Integer, String[]> getChildAges() {
		return ChildAges;
	}
	
	public void setChildAges(String childAges) {
		ChildAges = new HashMap<Integer, String[]>();
		String[] RoomChild = childAges.trim().split("/");
	    int Count = 1;
		for (String string : RoomChild) {
		ChildAges.put(Count, string.split(";"));
		Count++;
		}
	}
	
	public ArrayList<String> getRoomType() {
		return RoomTypes;
	}
	
	public void setRoomTypes(String roomTypes) {
		RoomTypes      = new ArrayList<String>();
		String[] Rooms = roomTypes.split("/");
	    
		for (String string : Rooms) {
		   RoomTypes.add(string);
	      }	
	
	}
	
	public String getDescription() {
		return Description;
	}
	
	public void setDescription(String description) {
		Description = description;
	}
	
	public String getExpected() {
		return Expected;
	}
	
	public void setExpected(String expected ) {
		Expected = expected;
	}
	
	public void setExpected(String expected,String Rate) {
		Double ExpectedValue = Double.parseDouble(expected)*Double.parseDouble(Rate);
		DecimalFormat     df = new DecimalFormat("#.##");
		ExpectedValue        = Double.valueOf(df.format(ExpectedValue));
	    Expected             = Double.toString(ExpectedValue);
	}
	
	public String getArrivalDate() {
		return ArrivalDate;
	}
	public void setArrivalDate(String arrivalDate) {
		ArrivalDate = arrivalDate;
	}

	public CancellationPolicy getCartPolicy() {
		return CartPolicy;
	}

	public void setCartPolicy(CancellationPolicy cartPolicy) {
		CartPolicy = cartPolicy;
	}


	



  

}
