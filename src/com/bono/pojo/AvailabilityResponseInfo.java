package com.bono.pojo;

import java.util.ArrayList;

public class AvailabilityResponseInfo {
	
	private String availability_Note;
	private String availability_URL;
	private String hotel_id;
	private String total_tax;
	private String total_RoomRate;
	private ArrayList<String> room_No;
	private ArrayList<String> total_rate;
	private ArrayList<String> room_code;
	private ArrayList<String> room_typecode;
	private ArrayList<String> bed_typecode;
	private ArrayList<String> Rate_plancode;
	private String pageSource;
	private String errorCode;
	private String errorDiscription;
	private long elapsedtime;
	private String BenchMarkContent;
	private String status;
	private int hotelcount;
	private int benchmarkCount;
	private String response;
	private ArrayList<String> changeRoom_room_No;
	private ArrayList<String> changeRoom_total_rate;
	private ArrayList<String> changeRoom_room_code;
	private ArrayList<String> changeRoom_room_typecode;
	private ArrayList<String> changeRoom_room_type;
	private ArrayList<String> changeRoom_bed_typecode;
	private ArrayList<String> changeRoom_bed_type;
	private ArrayList<String> changeRoom_Rate_plancode;
	private ArrayList<String> changeRoom_Rate_plan;
	
	
	public ArrayList<String> getRoom_No() {
		return room_No;
	}
	public void setRoom_No(ArrayList<String> room_No) {
		this.room_No = room_No;
	}
	public ArrayList<String> getChangeRoom_room_No() {
		return changeRoom_room_No;
	}
	public void setChangeRoom_room_No(ArrayList<String> changeRoom_room_No) {
		this.changeRoom_room_No = changeRoom_room_No;
	}
	public ArrayList<String> getChangeRoom_total_rate() {
		return changeRoom_total_rate;
	}
	public void setChangeRoom_total_rate(ArrayList<String> changeRoom_total_rate) {
		this.changeRoom_total_rate = changeRoom_total_rate;
	}
	public ArrayList<String> getChangeRoom_room_code() {
		return changeRoom_room_code;
	}
	public void setChangeRoom_room_code(ArrayList<String> changeRoom_room_code) {
		this.changeRoom_room_code = changeRoom_room_code;
	}
	public ArrayList<String> getChangeRoom_room_typecode() {
		return changeRoom_room_typecode;
	}
	public void setChangeRoom_room_typecode(
			ArrayList<String> changeRoom_room_typecode) {
		this.changeRoom_room_typecode = changeRoom_room_typecode;
	}
	public ArrayList<String> getChangeRoom_bed_typecode() {
		return changeRoom_bed_typecode;
	}
	public void setChangeRoom_bed_typecode(ArrayList<String> changeRoom_bed_typecode) {
		this.changeRoom_bed_typecode = changeRoom_bed_typecode;
	}
	public ArrayList<String> getChangeRoom_Rate_plancode() {
		return changeRoom_Rate_plancode;
	}
	public void setChangeRoom_Rate_plancode(
			ArrayList<String> changeRoom_Rate_plancode) {
		this.changeRoom_Rate_plancode = changeRoom_Rate_plancode;
	}
	public String getPageSource() {
		return pageSource;
	}
	public void setPageSource(String pageSource) {
		this.pageSource = pageSource;
	}
	public String getAvailability_Note() {
		return availability_Note;
	}
	public void setAvailability_Note(String availability_Note) {
		this.availability_Note = availability_Note;
	}
	public String getAvailability_URL() {
		return availability_URL;
	}
	public void setAvailability_URL(String availability_URL) {
		this.availability_URL = availability_URL;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorDiscription() {
		return errorDiscription;
	}
	public void setErrorDiscription(String errorDiscription) {
		this.errorDiscription = errorDiscription;
	}
	public long getElapsedtime() {
		return elapsedtime;
	}
	public void setElapsedtime(long elapsedtime) {
		this.elapsedtime = elapsedtime;
	}
	public String getBenchMarkContent() {
		return BenchMarkContent;
	}
	public void setBenchMarkContent(String benchMarkContent) {
		BenchMarkContent = benchMarkContent;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getHotelcount() {
		return hotelcount;
	}
	public void setHotelcount(int hotelcount) {
		this.hotelcount = hotelcount;
	}
	public int getBenchmarkCount() {
		return benchmarkCount;
	}
	public void setBenchmarkCount(int benchmarkCount) {
		this.benchmarkCount = benchmarkCount;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getHotel_id() {
		return hotel_id;
	}
	public void setHotel_id(String hotel_id) {
		this.hotel_id = hotel_id;
	}
	
	public String getTotal_RoomRate() {
		return total_RoomRate;
	}
	public void setTotal_RoomRate(String total_RoomRate) {
		this.total_RoomRate = total_RoomRate;
	}
	public ArrayList<String> getTotal_rate() {
		return total_rate;
	}
	public void setTotal_rate(ArrayList<String> total_rate) {
		this.total_rate = total_rate;
	}
	public String getTotal_tax() {
		return total_tax;
	}
	public void setTotal_tax(String total_tax) {
		this.total_tax = total_tax;
	}
	public ArrayList<String> getRoom_code() {
		return room_code;
	}
	public void setRoom_code(ArrayList<String> room_code) {
		this.room_code = room_code;
	}
	public ArrayList<String> getRoom_typecode() {
		return room_typecode;
	}
	public void setRoom_typecode(ArrayList<String> room_typecode) {
		this.room_typecode = room_typecode;
	}
	public ArrayList<String> getBed_typecode() {
		return bed_typecode;
	}
	public void setBed_typecode(ArrayList<String> bed_typecode) {
		this.bed_typecode = bed_typecode;
	}
	public ArrayList<String> getRate_plancode() {
		return Rate_plancode;
	}
	public void setRate_plancode(ArrayList<String> rate_plancode) {
		Rate_plancode = rate_plancode;
	}
	public ArrayList<String> getChangeRoom_room_type() {
		return changeRoom_room_type;
	}
	public void setChangeRoom_room_type(ArrayList<String> changeRoom_room_type) {
		this.changeRoom_room_type = changeRoom_room_type;
	}
	public ArrayList<String> getChangeRoom_bed_type() {
		return changeRoom_bed_type;
	}
	public void setChangeRoom_bed_type(ArrayList<String> changeRoom_bed_type) {
		this.changeRoom_bed_type = changeRoom_bed_type;
	}
	public ArrayList<String> getChangeRoom_Rate_plan() {
		return changeRoom_Rate_plan;
	}
	public void setChangeRoom_Rate_plan(ArrayList<String> changeRoom_Rate_plan) {
		this.changeRoom_Rate_plan = changeRoom_Rate_plan;
	}
	
	

}
