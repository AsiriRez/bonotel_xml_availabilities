package com.bono.pojo;

import types.ChargeByType;


public class CartDetails {
	String HotelLable                  = "";
	String rate                        = "";
	String SubTotal                    = "";
	String Taxes                       = "";
	String CartCurrency                = "";
	String TotaPaybel                  = "";
	
	
	//////////////////////////////////////////////////////////
	String        ServiceFee                  = "";
	double        NetRate                     = 0.0;
	double        TaxesAndOtherCharges        = 0.0;
	ChargeByType  ServiceFeeType              = ChargeByType.NONE;
	String        TotalHotelTaxes             = "";
	double        TotalHotelTaxesinBase           ;

	//////////////////////////////////////////////////////////
	boolean isRateChangeAlertAvailable        = false;
	String  RateChangeTitle                   = "-";
	String  RateChangeText                    = "-";
	boolean IsAddedToCart                     = false;
	String  AddToCartTime                     = "";
	boolean isSameHoteAlertAvailable          = false;
	String  SameHotelTitle                    = "-";
	String  SameHotelText                     = "-";
	boolean isRateGotChangeAlertAvailable     = false;
	String  RateGotChangeTitle                = "-";
	String  RateGotChangeText                 = "-";
	
//////////////////////////////////////////////////////////
	double  CalculatedServiceFeeInSelling     = 0.0;

	
	
	public double getTotalHotelTaxesinBase() {
		return TotalHotelTaxesinBase;
	}
	public void setTotalHotelTaxesinBase(double totalHotelTaxesinBase) {
		TotalHotelTaxesinBase = totalHotelTaxesinBase;
	}
	
	
	public double getTaxesAndOtherCharges() {
		return TaxesAndOtherCharges;
	}
	public void setTaxesAndOtherCharges(double taxesAndOtherCharges) {
		TaxesAndOtherCharges = taxesAndOtherCharges;
	}
	public boolean isRateGotChangeAlertAvailable() {
		return isRateGotChangeAlertAvailable;
	}
	public void setRateGotChangeAlertAvailable(boolean isRateGotChangeAlertAvailable) {
		this.isRateGotChangeAlertAvailable = isRateGotChangeAlertAvailable;
	}
	public String getRateGotChangeTitle() {
		return RateGotChangeTitle;
	}
	public void setRateGotChangeTitle(String rateGotChangeTitle) {
		RateGotChangeTitle = rateGotChangeTitle;
	}
	public String getRateGotChangeText() {
		return RateGotChangeText;
	}
	public void setRateGotChangeText(String rateGotChangeText) {
		RateGotChangeText = rateGotChangeText;
	}
	public double getCalculatedServiceFeeInSelling() {
		return CalculatedServiceFeeInSelling;
	}
	public void setCalculatedServiceFeeInSelling(
			double calculatedServiceFeeInSelling) {
		CalculatedServiceFeeInSelling = calculatedServiceFeeInSelling;
	}
	public String getTotalHotelTaxes() {
		return TotalHotelTaxes;
	}
	public void setTotalHotelTaxes(String totalHotelTaxes) {
		TotalHotelTaxes = totalHotelTaxes;
	}
	public ChargeByType getServiceFeeType() {
		return ServiceFeeType;
	}
	public void setServiceFeeType(String serviceFeeType) {
		ServiceFeeType = ChargeByType.getChargeType(serviceFeeType);
	}
	public double getNetRate() {
		return NetRate;
	}
	public void setNetRate(String netRate) {
		NetRate = Double.parseDouble(netRate);
	}
	public String getTotaPaybel() {
		return TotaPaybel;
	}
	public void setTotaPaybel(String totalPayble) {
		TotaPaybel = totalPayble;
	}
	public boolean isSameHoteAlertAvailable() {
		return isSameHoteAlertAvailable;
	}
	public void setSameHoteAlertAvailable(boolean isSameHoteAlertAvailable) {
		this.isSameHoteAlertAvailable = isSameHoteAlertAvailable;
	}
	public String getSameHotelTitle() {
		return SameHotelTitle;
	}
	public void setSameHotelTitle(String sameHotelTitle) {
		SameHotelTitle = sameHotelTitle;
	}
	public String getSameHotelText() {
		return SameHotelText;
	}
	public void setSameHotelText(String sameHotelText) {
		SameHotelText = sameHotelText;
	}
	public String getCartCurrency() {
		return CartCurrency;
	}
	public void setCartCurrency(String cartCurrency) {
		CartCurrency = cartCurrency;
	}
	public String getAddToCartTime() {
		return AddToCartTime;
	}
	public void setAddToCartTime(String addToCartTime) {
		AddToCartTime = addToCartTime;
	}
	public boolean isRateChangeAlertAvailable() {
		return isRateChangeAlertAvailable;
	}
	public void setRateChangeAlertAvailable(boolean isRateChangeAlertAvailable) {
		this.isRateChangeAlertAvailable = isRateChangeAlertAvailable;
	}
	public String getRateChangeTitle() {
		return RateChangeTitle;
	}
	public void setRateChangeTitle(String rateChangeTitle) {
		RateChangeTitle = rateChangeTitle;
	}
	public String getRateChangeText() {
		return RateChangeText;
	}
	public void setRateChangeText(String rateChangeText) {
		RateChangeText = rateChangeText;
	}
	public boolean isIsAddedToCart() {
		return IsAddedToCart;
	}
	public void setIsAddedToCart(boolean isAddedToCart) {
		IsAddedToCart = isAddedToCart;
	}
	public String getHotelLable() {
		return HotelLable;
	}
	public void setHotelLable(String hotelLable) {
		HotelLable = hotelLable;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getSubTotal() {
		return SubTotal;
	}
	public void setSubTotal(String subTotal) {
		SubTotal = subTotal;
	}
	public String getTaxes() {
		return Taxes;
	}
	public void setTaxes(String taxes) {
		Taxes = taxes;
	}
	public String getServiceFeeInPortalCurrency() {
		return ServiceFee;
	}
	public void setServiceFeeInPortalCurrency(String serviceFee) {
		ServiceFee = serviceFee;
	}
	
	
	

}
