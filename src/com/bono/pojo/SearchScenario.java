package com.bono.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class SearchScenario {
	
	private String scenario = "";
	private String searchBy = "";
	private String code = "";
	private String checkInDate = "";
	private String checkOutDate = "";
	private String noOfRooms = "";
	private String noOfNights = "";
	private String roomAD = "";
	private String roomCH = "";
	private String childAges1 = "";	
	private String childAges2 = "";	
	private String childAges3 = "";	
	private String childAges4 = "";
	private String upTo = "";
	private String touroperator = "";
	private String password = "";
	private String validationType = "";
	private String modificationType = "";
	private String stayPeriod_FromDate = "";
	private String stayPeriod_ToDate = "";
	private List<String> stayPeriod_occupancyList;
	private Map<String, SearchType> Searchmap;
	private   Map<String, String>   DefaultRooms                                              = new HashMap<String, String>();
	private   Map<String, String>   SelectedRooms                                             = new HashMap<String, String>();
	private ArrayList<RoomType>                SelectedRoomObj                      = new ArrayList<RoomType>();
	private Map<String, ArrayList<RoomType>>   ResultsRoomMap                       = new HashMap<String, ArrayList<RoomType>>();
	private String   TotalRate             = "";
	
	
	public String getTotalRate() {
		return TotalRate;
	}
	public void setTotalRate(String totalRate) {
		TotalRate = totalRate;
	}
	public void addToResultsRoomMap(ArrayList<RoomType> RoomList, String Room) {
		ResultsRoomMap.put(Room, RoomList);
	}
	public Map<String, ArrayList<RoomType>> getResultsRoomMap() {
		return ResultsRoomMap;
	}

	public void setResultsRoomMap(Map<String, ArrayList<RoomType>> resultsRoomMap) {
		ResultsRoomMap = resultsRoomMap;
	}

	public void setDefaultRooms(Map<String, String> defaultRooms) {
		DefaultRooms = defaultRooms;
	}

	public void setSelectedRooms(Map<String, String> selectedRooms) {
		SelectedRooms = selectedRooms;
	}

	public void setSelectedRoomObj(ArrayList<RoomType> selectedRoomObj) {
		SelectedRoomObj = selectedRoomObj;
	}

	public ArrayList<RoomType> getSelectedRoomObj() {
		return SelectedRoomObj;
	}

	public void addToSelectedRoomObj(RoomType selectedRoom) {
		SelectedRoomObj.add(selectedRoom);
	}
	public Map<String, String> getSelectedRooms() {
		return SelectedRooms;
	}

	public void addToSelectedRooms(String roomnum, String RoomString) {
		SelectedRooms.put(roomnum, RoomString);
	}
	public Map<String, String> getDefaultRooms() {
		return DefaultRooms;
	}

	public void addToDefaultRooms(String roomnum, String RoomString) {
		DefaultRooms.put(roomnum, RoomString);
	}
	public List<String> getStayPeriod_occupancyList() {
		return stayPeriod_occupancyList;
	}
	public void setStayPeriod_occupancyList(
			List<String> occupancyList) {
		this.stayPeriod_occupancyList = occupancyList;
	}
	public String getStayPeriod_FromDate() {
		return stayPeriod_FromDate;
	}
	public void setStayPeriod_FromDate(String stayPeriod_FromDate) {
		this.stayPeriod_FromDate = stayPeriod_FromDate;
	}
	public String getStayPeriod_ToDate() {
		return stayPeriod_ToDate;
	}
	public void setStayPeriod_ToDate(String stayPeriod_ToDate) {
		this.stayPeriod_ToDate = stayPeriod_ToDate;
	}
	public String getModificationType() {
		return modificationType;
	}
	public void setModificationType(String modificationType) {
		this.modificationType = modificationType;
	}
	public String getChildAges1() {
		return childAges1;
	}
	public void setChildAges1(String childAges1) {
		this.childAges1 = childAges1;
	}
	
	public String getChildAges2() {
		return childAges2;
	}
	public void setChildAges2(String childAges2) {
		this.childAges2 = childAges2;
	}
	
	public String getChildAges3() {
		return childAges3;
	}
	public void setChildAges3(String childAges3) {
		this.childAges3 = childAges3;
	}
	
	public String getChildAges4() {
		return childAges4;
	}
	public void setChildAges4(String childAges4) {
		this.childAges4 = childAges4;
	}
	public String getSearchBy() {
		return searchBy;
	}
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCheckInDate() {
		return checkInDate;
	}
	public void setCheckInDate(String checkInDate) {
		this.checkInDate = checkInDate;
	}
	public String getCheckOutDate() {
		return checkOutDate;
	}
	public void setCheckOutDate(String checkOutDate) {
		this.checkOutDate = checkOutDate;
	}
	public String getNoOfRooms() {
		return noOfRooms;
	}
	public void setNoOfRooms(String noOfRooms) {
		this.noOfRooms = noOfRooms;
	}
	public String getNoOfNights() {
		return noOfNights;
	}
	public void setNoOfNights(String noOfNights) {
		this.noOfNights = noOfNights;
	}
	public String getRoomAD() {
		return roomAD;
	}
	public void setRoomAD(String roomAD) {
		this.roomAD = roomAD;
	}
	public String getRoomCH() {
		return roomCH;
	}
	public void setRoomCH(String roomCH) {
		this.roomCH = roomCH;
	}
	
	public String getUpTo() {
		return upTo;
	}
	public void setUpTo(String upTo) {
		this.upTo = upTo;
	}
	public String getTouroperator() {
		return touroperator;
	}
	public void setTouroperator(String touroperator) {
		this.touroperator = touroperator;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getValidationType() {
		return validationType;
	}
	public void setValidationType(String validationType) {
		this.validationType = validationType;
	}
	public Map<String, SearchType> getSearchmap() {
		return Searchmap;
	}
	public void setSearchmap(Map<String, SearchType> searchmap) {
		Searchmap = searchmap;
	}
	public String getScenario() {
		return scenario;
	}
	public void setScenario(String scenario) {
		this.scenario = scenario;
	}
	
	
}
