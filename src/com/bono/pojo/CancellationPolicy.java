package com.bono.pojo;

import java.util.ArrayList;

public class CancellationPolicy {
	
private String            Numberofpolicies    ="";
private ArrayList<String> policies            = null;
private String            DeadlineString      = "";
private String            Refundable          = "";
private String            Deadline            = "";
private boolean           WithinCancellation      ;




public String getDeadlineString() {
	return DeadlineString;
}

public void setDeadlineString(String deadlineString) {
	DeadlineString = deadlineString;
}

public boolean isWithinCancellation() {
	return WithinCancellation;
}

public void setWithinCancellation(boolean withinCancellation) {
	WithinCancellation = withinCancellation;
}

public void setPolicies(ArrayList<String> policies) {
	this.policies    = policies;
	Numberofpolicies = Integer.toString(policies.size()); 
}

public CancellationPolicy()
{
	policies = new ArrayList<String>();
}

public String getNumberofpolicies() {
	return Numberofpolicies;
}
public void setNumberofpolicies(String numberofpolicies) {
	Numberofpolicies = numberofpolicies;
}
public ArrayList<String> getPolicies() {
	return policies;
}
public void addPolicies(String e) {
	this.policies.add(e);
}
public String getDeadline() {
	return Deadline;
}
public void setDeadline(String deadline) {
	Deadline = deadline;
}
public String getRefundable() {
	return Refundable;
}
public void setRefundable(String refundable) {
	Refundable = refundable;
}





}
