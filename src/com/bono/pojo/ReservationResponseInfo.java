package com.bono.pojo;

import java.util.ArrayList;

public class ReservationResponseInfo {
	
	private String bookingNo;
	private String totalBookingValue;
	
	private String errorCode;
	private String errorDescription;
	private long elapsedtime;
	private String status;
	private String url;
	private String node;
	private ArrayList<CancellationResponseInfo> cancelList;
	private ArrayList<String> roomResNo;
	
	
	
	public ArrayList<String> getRoomResNo() {
		return roomResNo;
	}

	public void setRoomResNo(ArrayList<String> roomResNo) {
		this.roomResNo = roomResNo;
	}

	public ArrayList<CancellationResponseInfo> getCancelList() {
		return cancelList;
	}

	public void setCancelList(ArrayList<CancellationResponseInfo> cancelList) {
		this.cancelList = cancelList;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	

	public long getElapsedtime() {
		return elapsedtime;
	}

	public void setElapsedtime(long elapsedtime) {
		this.elapsedtime = elapsedtime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBookingNo() {
		return bookingNo;
	}

	public void setBookingNo(String bookingNo) {
		this.bookingNo = bookingNo;
	}

	public String getTotalBookingValue() {
		return totalBookingValue;
	}

	public void setTotalBookingValue(String totalBookingValue) {
		this.totalBookingValue = totalBookingValue;
	}
	
	

}
