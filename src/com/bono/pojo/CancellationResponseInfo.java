package com.bono.pojo;

public class CancellationResponseInfo {

	private String cancellationNo;
	private String cancellation_Fee;
	
	private String errorCode;
	private String errorDescription;
	private long elapsedtime;
	private String status;
	private String url;
	private String node;
	
	
	
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	public long getElapsedtime() {
		return elapsedtime;
	}
	public void setElapsedtime(long elapsedtime) {
		this.elapsedtime = elapsedtime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getNode() {
		return node;
	}
	public void setNode(String node) {
		this.node = node;
	}
	public String getCancellationNo() {
		return cancellationNo;
	}
	public void setCancellationNo(String cancellationNo) {
		this.cancellationNo = cancellationNo;
	}
	public String getCancellation_Fee() {
		return cancellation_Fee;
	}
	public void setCancellation_Fee(String cancellation_Fee) {
		this.cancellation_Fee = cancellation_Fee;
	}
	
	
	
}
