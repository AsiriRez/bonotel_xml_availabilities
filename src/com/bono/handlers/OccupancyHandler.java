package com.bono.handlers;

import java.util.ArrayList;

import com.bono.pojo.SearchScenario;

/**
 * @author Dulan
 *
 */

public class OccupancyHandler {

	private String NumOfRooms;
	private String[] adultsList;
	private String[] childList;
	private String room1chages;
	private String room2chages;
	private String room3chages;
	private String room4chages;
	private String detail = "<roomInfo><roomTypeId>0</roomTypeId><bedTypeId>0</bedTypeId><adultsNum>adnum</adultsNum><childNum>chnum</childNum>filldetails</roomInfo>";
	private String childinfo1 = "<childAges><childAge>age1</childAge></childAges>";
	private String childinfo2 = "<childAges><childAge>age1</childAge><childAge>age2</childAge></childAges>";
	
	
	public OccupancyHandler(SearchScenario searchObject) {
		
		NumOfRooms = searchObject.getNoOfRooms();
		adultsList = searchObject.getRoomAD().split("/");
		childList = searchObject.getRoomCH().split("/");		
		room1chages = searchObject.getChildAges1();		
		room2chages = searchObject.getChildAges2();		
		room3chages = searchObject.getChildAges3();		
		room4chages = searchObject.getChildAges4();
		
		if (room1chages.length() == 1) {
			room1chages = room1chages.concat(";0");
		}
		if (room2chages.length() == 1) {
			room2chages = room2chages.concat(";0");
		}
		if (room3chages.length() == 1) {
			room3chages = room3chages.concat(";0");
		}
		if (room4chages.length() == 1) {
			room4chages = room4chages.concat(";0");
		}

	}

	public String getOccupancy() {
		
		try {
			
			ArrayList<String> sendReq = new ArrayList<String>();
			ArrayList<String> childAges = new ArrayList<String>();
			childAges.add(room1chages);
			childAges.add(room2chages);
			childAges.add(room3chages);
			childAges.add(room4chages);
			
			for (int i = 1; i <= Integer.parseInt(NumOfRooms); i++) {
				String[] ages = childAges.get(i-1).split(";");
				String send = roomHandler(adultsList[i-1], childList[i-1], ages[0], ages[1]);
				sendReq.add(send);
			}
			
			String occupancy = "";
			for (int i = 0; i< sendReq.size(); i++) {
				occupancy = occupancy.concat(sendReq.get(i));				
			}
			
			return occupancy;
				
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	public String roomHandler(String Adult, String child, String Age1, String Age2) {
		
		String intern = (detail.replace("adnum", Adult)).replace("chnum", child);
		if (child.equals("0"))
			return intern.replace("filldetails", "");
		else if (child.equals("1"))
			return intern.replace("filldetails", childinfo1.replace("age1", Age1));
		else
			return intern.replace("filldetails", (childinfo2.replace("age1", Age1)).replace("age2", Age2));

	}

	
}
