package com.bono.handlers;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;

import org.apache.log4j.Logger;
//import dynamic.response.IncomingResponse;

import com.bono.util.HtmlWriter;
import com.bono.util.PG_Properties;






public class HttpRequestHandler {
	

	private final String USER_AGENT = "Mozilla/5.0";
	private Logger HttpRequestLogger ;
	private String UUID;

 

	
	
	public HttpRequestHandler(String UID)
	{
	
		UUID = UID;
		HttpRequestLogger = Logger.getLogger(HttpRequestHandler.class.getName());
	}
	

	public void processRequest(String Request,String CurrentUrl)
	{
		
		HttpRequestLogger.info("Initializing Response Extractor");
	
		
		try {
			HttpRequestLogger.info("Calling Send Post With Parameters   Request --->"+Request+ ":Url--->"+CurrentUrl);
			String Response  = this.sendPost(Request,CurrentUrl);
			
		} catch (Exception e) {
			HttpRequestLogger.fatal(e.toString());
		}
		
		
	}
	
	
	
	public String sendPost(String Request,String URL) throws Exception {
		

		if(PG_Properties.getProperty("Proxy.Use").equals("true"))
		{
			System.setProperty("proxySet", "true");
			System.setProperty("http.proxyHost", PG_Properties.getProperty("Proxy.Url"));
			System.setProperty("http.proxyPort", PG_Properties.getProperty("Proxy.Port"));
			Authenticator.setDefault(new Authenticator() {
			    protected PasswordAuthentication getPasswordAuthentication() {

			        return new PasswordAuthentication(PG_Properties.getProperty("Proxy.User"),PG_Properties.getProperty("Proxy.Pass").toCharArray());
			    }
			});
		}
		else
		{
			System.setProperty("proxySet", "false");
		}
	
		 
		String url =URL;
        URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		
		String urlParameters = Request;
		urlParameters        = urlParameters.replace("availabilityRequestcancelpolicy", "availabilityRequest  cancelpolicy");
		urlParameters        = urlParameters.replace("reservationRequestreturnCompeleteBookingDetails", "reservationRequest returnCompeleteBookingDetails");
		urlParameters        = urlParameters.replace("totalcurrency", "total currency");
		urlParameters        = urlParameters.replace("totalTaxcurrency", "totalTax currency");
		urlParameters        = urlParameters.replace("reservationDetailstimeStamp", "reservationDetails timeStamp");
		urlParameters        = urlParameters.replace("cancellationRequesttimestamp", "cancellationRequest timestamp");
		urlParameters        = urlParameters.replace("modifyReservationDetailstimeStamp=\"20100428T15:25:50\"modifyType", "modifyReservationDetails timeStamp=\"20100428T15:25:50\" modifyType");
		
/*		 urlParameters ="xml=<availabilityRequest cancelpolicy=\"Y\""
				+"><control><userName>TestAutoLive_xml</userName><passWord>testauto</passWord></control><checkIn>18-Jan-2015</checkIn><checkOut>22-Jan-"
				+"2015</checkOut><noOfRooms>2</noOfRooms><noOfNights>4</noOfNights><country>US</country><state>NV</state><hotelCodes><hotelCode>3337</hote"
				+"lCode></hotelCodes><roomsInformation><roomInfo><roomTypeId>0</roomTypeId><bedTypeId>0</bedTypeId><adultsNum>1</adultsNum><childNum>0</ch"
				+"ildNum></roomInfo><roomInfo><roomTypeId>0</roomTypeId><bedTypeId>0</bedTypeId><adultsNum>2</adultsNum><childNum>1</childNum><childAges><"
				+"childAge>07</childAge></childAges></roomInfo></roomsInformation></availabilityRequest>";*/
 
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
        HtmlWriter.setToDefault();
  
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);
 
		BufferedReader in = new BufferedReader(
	    new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		//print result
	//	System.out.println(response.toString());
		
		  BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(PG_Properties.getProperty("Log.Path").concat("/"+UUID+"_Response.html"))));
		  bwr.write(response.toString());
		  bwr.flush();
		  bwr.close();
		 // System.out.println("Written to the file");
		
         return response.toString();
	}
	

	
	

}
