package com.bono.handlers;
/**
 * @author Dulan
 *
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.bono.pojo.SearchScenario;


public class RoomDetailHandler {

	private String[] adultsList;
	private String[] childList;
	

	public List<String> getOccupancy(SearchScenario searchObject){
		
		ArrayList<String> list = new ArrayList<String>();
		adultsList = searchObject.getRoomAD().split("/");
		childList = searchObject.getRoomCH().split("/");		
		String R1_array[] = searchObject.getChildAges1().split(";");
		String R2_array[] = searchObject.getChildAges2().split(";");
		String R3_array[] = searchObject.getChildAges3().split(";");
		String R4_array[] = searchObject.getChildAges4().split(";");
		
		for (int i = 1; i <= Integer.parseInt(searchObject.getNoOfRooms()); i++) {
			
			if (i == 1) {
				String room1 = roomFill(Integer.parseInt(adultsList[i-1]),Integer.parseInt(childList[i-1]), R1_array);
				list.add(room1);
			}else if(i == 2){
				String room2 = roomFill(Integer.parseInt(adultsList[i-1]),Integer.parseInt(childList[i-1]), R2_array);
				list.add(room2);
			}else if(i == 3){
				String room3 = roomFill(Integer.parseInt(adultsList[i-1]),Integer.parseInt(childList[i-1]), R3_array);
				list.add(room3);
			}else{
				String room4 = roomFill(Integer.parseInt(adultsList[i-1]),Integer.parseInt(childList[i-1]), R4_array);
				list.add(room4);
			}			
		}
		
		return list;

	}

	
	public String roomFill(int Adult, int Child, String childages[]) {
	
		String AdultStream = "<guest><title>Mr</title><firstName>f_name</firstName><lastName>TEST</lastName></guest>";
		String ChildStream = "<guest><title>Child.</title><firstName>CF_name</firstName><lastName>Test</lastName><age>C_age</age></guest>";
		
		String ADstream = "";
		String CHstream = "";

		for (int i = 1; i <= Adult; i++){
			
			Random rand = new Random();	
			int randomValue = rand.nextInt();
			String AdultStream1 = AdultStream.replace("f_name", "test".concat(Integer.toString(randomValue)));
			ADstream = ADstream.concat(AdultStream1);
		}

		for (int i = 1; i <= Child; i++){
			
			Random rand = new Random();	
			int randomValue = rand.nextInt();
			String CHstream1 = ChildStream.replace("CF_name","Child".concat(Integer.toString(randomValue)));
			CHstream1 = CHstream1.replace("C_age", childages[i - 1]);
			CHstream = CHstream.concat(CHstream1);
		}

		String outputstream = ADstream.concat(CHstream);

		return outputstream;
	}

	
}
