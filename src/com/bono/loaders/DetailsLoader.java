package com.bono.loaders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;

import com.bono.pojo.Node;
import com.bono.pojo.RoomType;
import com.bono.pojo.SearchScenario;
import com.bono.pojo.SearchType;

public class DetailsLoader {

	
	public HashMap<String, SearchScenario> loadBonotelSearches(Map<Integer, String> searchInfoDeMap){
		
		int cnt=0;
		Iterator<Map.Entry<Integer, String>> it = searchInfoDeMap.entrySet().iterator();
		HashMap<String, SearchScenario> searchInfoMap = new HashMap<String, SearchScenario>();
		
		while(it.hasNext()) {
			
			SearchScenario search = new SearchScenario();
			String[] values = it.next().getValue().split(",");
			
			search.setScenario(values[0]);
			search.setSearchBy(values[1]);
			search.setCode(values[2]);
			search.setCheckInDate(values[3]);
			search.setCheckOutDate(values[4]);
			search.setNoOfRooms(values[5]);
			search.setNoOfNights(values[6]);			
			search.setRoomAD(values[7]);
			search.setRoomCH(values[8]);
			search.setChildAges1(values[9]);						
			search.setChildAges2(values[10]);						
			search.setChildAges3(values[11]);					
			search.setChildAges4(values[12]);			
			search.setUpTo(values[13]);
			search.setTouroperator(values[14]);
			search.setPassword(values[15]);
			search.setValidationType(values[16]);
			search.setModificationType(values[17]);
			search.setStayPeriod_FromDate(values[18]);
			search.setStayPeriod_ToDate(values[19]);
		    
			searchInfoMap.put(values[0], search);
			cnt++;
		}
		
		return searchInfoMap;
	}
	
	public TreeMap<Integer, Node> loadBonotel_Availabilities(Map<Integer, String> availabilityInfoDeMap){
		
		int cnt=0;
		Iterator<Map.Entry<Integer, String>> it = availabilityInfoDeMap.entrySet().iterator();
		TreeMap<Integer, Node> availInfoMap = new TreeMap<Integer, Node>();
		
		while(it.hasNext()) {
			
			Node availability = new Node();
			String[] values = it.next().getValue().split(",");
			
			availability.setNode(values[0]);
			availability.setURL(values[1]);
					    
			availInfoMap.put(cnt, availability);
			cnt++;
		}
		
		return availInfoMap;
	}
	
	public TreeMap<Integer,Node> loadBonotel_Reservations(Map<Integer, String> reservationInfoDeMap){
		
		int cnt=0;
		Iterator<Map.Entry<Integer, String>> it = reservationInfoDeMap.entrySet().iterator();
		TreeMap<Integer, Node> reservationInfoMap = new TreeMap<Integer,Node>();
		
		while(it.hasNext()) {
			
			Node reservation = new Node();
			String[] values = it.next().getValue().split(",");
			
			reservation.setNode(values[0]);
			reservation.setURL(values[1]);
					    
			reservationInfoMap.put(cnt, reservation);
			cnt++;
		}
		
		return reservationInfoMap;
	}
	
	public HashMap<String, SearchScenario> loadRoomDetails(Map<Integer, String> map, HashMap<String, SearchScenario> Searchmap) {

		Iterator<Map.Entry<String, SearchScenario>> it = Searchmap.entrySet().iterator();

		while (it.hasNext()) {
			Map.Entry<String, SearchScenario> CurrentEntry = it.next();
			String CurrentIndex = CurrentEntry.getKey();
			SearchScenario CurrentSearch = CurrentEntry.getValue();
			ArrayList<ArrayList<RoomType>> RoomListArray = new ArrayList<ArrayList<RoomType>>(Integer.parseInt(CurrentSearch.getNoOfRooms()));
			Double DefaultRoomRateTotal   = 0.0;
			Double SelectedRoomRateTotal  = 0.0;
			// ArrayList<ArrayList<RoomType>> RoomListArray = new
			// ArrayList<ArrayList<RoomType>>(3);

			for (int i = 0; i < Integer.parseInt(CurrentSearch.getNoOfRooms()); i++) {
				RoomListArray.add(new ArrayList<RoomType>());

			}

			Iterator<Map.Entry<Integer, String>> rawit = map.entrySet().iterator();

			while (rawit.hasNext()) {
				String[] AllValues = rawit.next().getValue().split(",");

				if (AllValues[0].equalsIgnoreCase(CurrentIndex)) {
					RoomType room = new RoomType();
					int RoomNum = Integer.parseInt(AllValues[1]);
					//room.setRoomLable(AllValues[2]);
					room.setRoomType(AllValues[2]);
					room.setBedType(AllValues[3]);
					room.setRateType(AllValues[4]);
					room.setDailyRates(new ArrayList<String>(Arrays.asList(AllValues[5].split(";"))));
					room.setTotalRate(AllValues[6]);
					//room.setCurrency(AllValues[8]);
					room.setPromotionApplied((AllValues[9].trim().equalsIgnoreCase("yes") ? true : false));
					room.setPromoCode(AllValues[10].trim());

					if (AllValues[7].trim().equalsIgnoreCase("Yes")){
						CurrentSearch.addToDefaultRooms(Integer.toString(RoomNum), room.getRoomType() + "/" + room.getBedType() + "/" + room.getRateType());
						DefaultRoomRateTotal  += Double.parseDouble(room.getTotalRate());
					}
					if (AllValues[8].trim().equalsIgnoreCase("Yes")) {
						CurrentSearch.addToSelectedRooms(Integer.toString(RoomNum), room.getRoomType() + "/" + room.getBedType() + "/" + room.getRateType());
						CurrentSearch.addToSelectedRoomObj(room);
						SelectedRoomRateTotal += Double.parseDouble(room.getTotalRate());
					}
					try {
						// RoomListArray.get(1);
						RoomListArray.get(RoomNum - 1).add(room);
					} catch (Exception e) {
						System.out.println("sds");
					}

				}
			}

			int count = 1;
			for (ListIterator<ArrayList<RoomType>> iterator = RoomListArray.listIterator(); iterator.hasNext();) {
				try {
					ArrayList<RoomType> arrayList = (ArrayList<RoomType>) iterator.next();
					CurrentSearch.addToResultsRoomMap(arrayList, Integer.toString(count));
					// Searchmap.remove(CurrentIndex);
					Searchmap.put(CurrentIndex, CurrentSearch);
				} catch (Exception e) {
					System.out.println("Errorrrrr");
				}

				count++;

			}
			CurrentSearch.setTotalRate(new BigDecimal(Math.ceil(DefaultRoomRateTotal)).stripTrailingZeros().toPlainString());
			
	//		System.out.println(CurrentSearch.getTotalRate());
			
		}
		
		
		return Searchmap;

	}
}
