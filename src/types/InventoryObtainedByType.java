package types;

public enum InventoryObtainedByType {

	BYHOTEL,BYROOM,BYROOMBED,NONE;
	
	public static InventoryObtainedByType getinveType(String ChargeType)
	{
		if(ChargeType.equalsIgnoreCase("Hotel"))return BYHOTEL;
		else if(ChargeType.equalsIgnoreCase("Room Type"))return BYROOM;
		else if(ChargeType.equalsIgnoreCase("Room and Bed Type"))return BYROOMBED;
		else return NONE;
		
	}



}
