package types;

public enum ChargeByType {
	
	PERCENTAGE,VALUE,NIGHTRATE,NONE;
	
	public static ChargeByType getChargeType(String ChargeType)
	{
		if(ChargeType.equalsIgnoreCase("Percentage"))return PERCENTAGE;
		else if(ChargeType.equalsIgnoreCase("Value"))return VALUE;
		else if(ChargeType.equalsIgnoreCase("Nights"))return NIGHTRATE;
		else return NONE;
		
	}

	
}
