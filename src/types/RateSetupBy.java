package types;

public enum RateSetupBy {

	PERROOM,PERPERSON,NONE;
	
	public static RateSetupBy getRateSetupType(String ChargeType)
	{
		if(ChargeType.equalsIgnoreCase("Per Room"))return PERROOM;
		else if(ChargeType.equalsIgnoreCase("Per Person"))return PERPERSON;
		else return NONE;

		
	}



}
